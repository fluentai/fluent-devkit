# Fluent.ai SDK dev kit

## About

In this repository, you will find sample code and demos to get started developing applications with the Fluent SDK.  

For these demos, you will require a copy of the Fluent SDK (library and header file).  Please contact fluent.ai to get a copy at sales@fluent.ai

# Examples / demos

The following sample applications are provided.  Please see the directories below for more info on each

## sdk_linux_demo

The objective of this example code is to show how to integrate the fluent SDK with the microphone to display detected wake-words and intents in real time on Linux.

## sdk_windows_demo

The objective of this example code is to show how to integrate the fluent SDK with the microphone to display detected wake-words and intents in real time on Windows.

## sdktest

This code allows you to evaluate performance offline on pre-recorded audio files.
