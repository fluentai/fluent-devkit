#!/usr/bin/env python3
import sys
import json

default_values = {
    "multiple_command_threshold": 0.3,
    "multiple_recording_min_duration_ms": 320,
    "multiple_early_stop_interval_ms": 320,
    "multiple_early_stop_repetitions": 3,
    "multiple_early_stop_threshold": 0.4
}

if len(sys.argv) != 2:
    print("Please provide a file path.")
    sys.exit(1)

filepath = sys.argv[1]

with open(filepath, "r") as json_file:
    data = json.load(json_file)

for key, value in default_values.items():
    if key not in data:
        data[key] = value

with open(filepath, "w") as json_file:
    json.dump(data, json_file, indent=4)
