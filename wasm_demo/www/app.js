
async function requestMicrophonePermission() {
  try {
    const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
    stream.getTracks().forEach((track) => track.stop());
    console.log('Microphone access granted');
    return true;
  } catch (error) {
    console.error('Microphone access denied', error);
    return false;
  }
}

let context = null;
let worklet = null;
let source = null;
let g_stream = null;

const handleStop = function (stream) {
  const tracks = stream.getTracks();
  tracks.forEach((track) => {
    track.stop();
  });
  worklet.disconnect();
  source.disconnect();
  context.close();
};

var obj_action_to_icons  = {
  music_play  : "P2_WW_08_Play.png",
  music_next  : "P2_WW_06_Next.png",
  music_pasue  : "P2_WW_07_Pause.png",
  music_previous  : "P2_WW_09_Previous.png",
  music_shuffle  : "P2_WW_10_Shuffle.png",
  music_stop     : "P2_WW_11_Stop.png",
  music_mute   : "P2_WW_04_Mute.png",
  music_unmute : "P2_WW_05_UnMute.png",
  volume_down      : "P2_WW_12_VolumeDown.png",
  volume_up      : "P2_WW_13_VolumeUp.png",
  call_answer : "P2_WW_02_Answer.png",
  call_decline : "P2_WW_03_Decline.png",
  device_pair : "P2_WW_01_Bluetooth.png",
  level_check_battery : "P2_WW_23_BatteryLevel.png",
  _current_mode : "P2_WW_18_CurrentMode.png",
  noise_cancelling_on : "P2_WW_17_NC_ON.png",
  noise_cancelling_off : "P2_WW_16_NC_OFF.png",
  volume_limit_on : "P2_WW_15_VolumeLimitON.png",
  volume_limit_off : "P2_WW_14_VolumeLimitOFF.png",
  config_one_activate : "P2_WW_19_Config1.png",
  config_two_activate : "P2_WW_20_Config2.png",
  photo_take : "P2_WW_22_TakePix.png",
  assistant_use : "P2_WW_21_Assistant.png"
};
//var outputContainer = null;
var app_state_img = null;

let row_splash = null;
let row_cmd = null;
let action_img = null;
let row_mic_permit = null;
let row_mic_deny = null;
let row_button = null;

function onMsgAudioWorklet(e) {
    let ret = e.data;
    switch(ret.status) {
      case 'FLUENTAI_STATE_WAITING':
        app_state_img.src = "./icons/Demo_TWS_PNGs_v1/P1_01_Waiting_4_WW.png";
        row_splash.style.display = "none";
        row_cmd.style.display = "flex";
        action_img.src = "./icons/Demo_TWS_PNGs_v1/P2_00_Background.png";
        break;
      case 'FLUENTAI_STATE_WAKE_WORD_DETECTED':
        if (!("command_id" in ret)) {
          app_state_img.src = "./icons/Demo_TWS_PNGs_v1/P1_02_Waiting_4_Command.png";
          action_img.src = "./icons/Demo_TWS_PNGs_v1/P2_00_WW.png";
        }
        else {
          console.log(ret.msg);
        }
        break;
      case 'FLUENTAI_STATE_COMMAND_DETECTED':
        app_state_img.src = "./icons/Demo_TWS_PNGs_v1/P1_01_Waiting_4_WW.png";
        key = ret.object + "_" + ret.action;
        if (key in obj_action_to_icons) {
          img_path = "./icons/Demo_TWS_PNGs_v1/" + obj_action_to_icons[key];
          action_img.src = img_path ;
        }
        break;
      case 'FLUENTAI_STATE_COMMAND_NOT_DETECTED':
        app_state_img.src = "./icons/Demo_TWS_PNGs_v1/P1_01_Waiting_4_WW.png";
        action_img.src = "./icons/Demo_TWS_PNGs_v1/P2_00_WW_AIR_Not.png";
        break;
      defaut:
        return;
    }
}

const handleSuccess = async function (stream) {
  g_stream = stream;
  context = new AudioContext({ sampleRate: 16000 });
  // https://github.com/GoogleChromeLabs/web-audio-samples/tree/main/src/audio-worklet/design-pattern/wasm/
  await context.audioWorklet.addModule(WASM_JS_FILE);
  source = context.createMediaStreamSource(stream);

  //await context.audioWorklet.addModule("processor.js");
  worklet = new AudioWorkletNode(context, "worklet-processor");
  worklet.port.onmessage =  onMsgAudioWorklet;
  source.connect(worklet);
  worklet.connect(context.destination);
};

function changeRecordButton(btn, state) {
  
  switch (state) {
    case "allow_mic":
      row_mic_permit.style.display = "block";
      break;
    case "wait_start":
      // P1
      app_state_img.src = "./icons/Demo_TWS_PNGs_v1/P1_00_Background.png";
      // P2
      row_splash.style.display = "block";
      row_cmd.style.display = "none";
      row_mic_permit.style.display = "none";
      row_mic_deny.style.display = "none";
      // P3
      row_button.style.display = "flex";
      btn.innerHTML =`<button class="h-100 w-100 border-0"   style="background-color: #b2b2b2;">
          <img class="mx-0 my-0" src="./icons/Demo_TWS_PNGs_v1/P3c_OFF.png"/>
        </button>
      `;
      console.log("Wait for start");
      break;
    case "wait_stop":
      row_splash.style.display = "block";
      row_cmd.style.display = "none";
      btn.innerHTML =`<button class="h-100 w-100 border-0"   style="background-color: #b2b2b2;">
          <img class="mx-0 my-0" src="./icons/Demo_TWS_PNGs_v1/P3c_ON.png"/>
        </button>
      `;
      console.log("Wait for stop");
      break;
    case "deny":
    default:
      row_mic_permit.style.display = "none";
      row_mic_deny.style.display = "block";
      break;
  }
  
}
// Let's start
// main
let prevSelectedValue = '';
window.onload = async function() {
  var recordButton = document.querySelector('#record');
  outputContainer = document.querySelector('.output');
  app_state_img = document.querySelector('#app_state');

  row_splash = document.querySelector('#row_splash');
  row_cmd = document.querySelector('#row_cmd');
  action_img = document.querySelector('#action_img ');
  row_mic_permit = document.querySelector('#row_mic_permit');
  row_mic_deny = document.querySelector('#row_mic_deny');
  row_button = document.querySelector('#row_button ');

  
  


  let micAuthorized = false;
  let micPermissionDenied = false;
  let micEnumerated = false;
  let recording = false;

  //changeRecordButton(recordButton, "permit");
  changeRecordButton(recordButton, "allow_mic");
  micAuthorized = await requestMicrophonePermission();
  if (!micAuthorized) {
    changeRecordButton(recordButton, "deny");
    return;
  }
  const selectElement = document.getElementById('microphone-select');
  if (!micEnumerated) {
    const devices = await navigator.mediaDevices.enumerateDevices();
    const micDevices = devices.filter(device => device.kind === 'audioinput');
    micDevices.forEach(device => {
      const option = document.createElement('option');
      option.value = device.deviceId;
      option.text = device.label;
      selectElement.add(option);
    });
    changeRecordButton(recordButton, "wait_start");
    micEnumerated = true;
  }
  //Let's start!!!!!!
  recordButton.addEventListener('click', async () => {
    if (!recording) {
      navigator.mediaDevices.getUserMedia({ audio: { deviceId: selectElement.value, }, video: false })
        .then(handleSuccess);
      changeRecordButton(recordButton, "wait_stop");
      recording = true;
    }
    else {
      handleStop(g_stream);
      changeRecordButton(recordButton, "wait_start");
      recording = false;
    }
  });
  prevSelectedValue = selectElement.value;

  selectElement.addEventListener('change', (event) => {
    const currentValue = event.target.value;
    if (currentValue !== prevSelectedValue && recording) {
      handleStop(g_stream);
      changeRecordButton(recordButton, "wait_start");
      recording = false;
      prevSelectedValue = currentValue;
    }
  });
};
