/*
 * Copyright 2023 Fluent.ai
 * All rights reserved.
 */
#include <cstdarg>
#include <cstdio>
#include <iostream>
#include "fluent/fluentai-sdk.h"
#include "fluent/log_model.hpp"

#include <emscripten/emscripten.h>
// https://github.com/GoogleChromeLabs/web-audio-samples/blob/main/src/audio-worklet/design-pattern/wasm/FluentaiKernel.cc
#include "emscripten/bind.h"
#include <emscripten.h>
#include <string>


#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN
#endif
EXTERN EMSCRIPTEN_KEEPALIVE void *malloc(size_t size);
EXTERN EMSCRIPTEN_KEEPALIVE void free(void *ptr);




uint16_t MSG_LENGTH = 160;

static void parse_command(char *p_command, char *p_action, char *p_object) {
  int str_index = 0;
  int substr_index = 0;
  enum parsing_state_e {
    PARSING_ACTION,
    PARSING_ACTION_FOUND,
    PARSING_OBJECT,
    PARSING_OBJECT_FOUND,
    PARSING_DONE
  } parsing_state = PARSING_ACTION;

  p_action[0] = '\0';
  p_object[0] = '\0';
  while (p_command[str_index] != '\0') {
    if ((p_command[str_index] == '"') || (p_command[str_index] == ' ')) {
      // Skip these characters
      str_index++;
      continue;
    }

    switch (parsing_state) {
    case PARSING_ACTION:
      if (p_command[str_index] == ':') {
        parsing_state = PARSING_ACTION_FOUND;
        substr_index = 0;
      }
      break;
    case PARSING_ACTION_FOUND:
      if ((p_command[str_index] == ',') || (p_command[str_index] == '}')) {
        parsing_state = PARSING_OBJECT;
        p_action[substr_index] = '\0';
      } else {
        p_action[substr_index++] = p_command[str_index];
      }
      break;
    case PARSING_OBJECT:
      if (p_command[str_index] == ':') {
        parsing_state = PARSING_OBJECT_FOUND;
        substr_index = 0;
      }
      break;
    case PARSING_OBJECT_FOUND:
      if (p_command[str_index] == '}') {
        parsing_state = PARSING_DONE;
        p_object[substr_index] = '\0';
      } else {
        p_object[substr_index++] = p_command[str_index];
      }
      break;
    default:
      // Nothing to do
      break;
    } // end switch
    str_index++;
  } // end while
}

using namespace emscripten;

const unsigned kRenderQuantumFrames = 128;
const unsigned kBytesPerChannel = kRenderQuantumFrames * sizeof(float);
class FluentaiKernel {
public:
  FluentaiKernel() {}
  ~FluentaiKernel() { fluentai_deleteInstance(&p_sdk); }

  void process(uintptr_t input_ptr, uintptr_t output_ptr, unsigned channel_count) {
    if (nullptr == p_sdk) {
      this->setup();
    }

    float *input_buffer = reinterpret_cast<float *>(input_ptr);
    float *output_buffer = reinterpret_cast<float *>(output_ptr);
    float audio_sum[kRenderQuantumFrames];
    for (unsigned channel = 0; channel < channel_count; ++channel) {
      float* destination = output_buffer + channel * kRenderQuantumFrames;
      float* source = input_buffer + channel * kRenderQuantumFrames;
      if ( channel == 0 ) {
        memcpy(audio_sum, source, kBytesPerChannel);
      }
      else {
        for (auto &v:audio_sum) {
          v += *source++;
        }
      }

    }
    static int16_t long_buf[640];
    float *in = audio_sum;
    static size_t pos_w = 0;
    static size_t pos_r = 0;
    for (int i = 0; i < kRenderQuantumFrames; i++) {
      long_buf[pos_w % 640] = *in /channel_count * 32768;
      pos_w++;
      in ++;
    }
    if (pos_w - pos_r < 160) {
      return;
    }
    auto *p_data = long_buf + (pos_r % 640);
    status = fluentai_processAudio(p_sdk, p_data, MSG_LENGTH);
    pos_r += 160;

    if (status == prev_status) {
      return;
    }
    val obj = val::object();

    switch (status) {
    case FLUENTAI_STATE_WAITING:
    case FLUENTAI_STATE_RECORDING_COMMAND:
      break;
    case FLUENTAI_STATE_ERROR: {
      const char *p_message = fluentai_getLastErrorMessage();
      printf("\nError processing audio : %s\r\n", p_message);
      break;
    }
    case FLUENTAI_STATE_WAKE_WORD_DETECTED: {
      fluentai_detection_result result;
      fluentai_getDetectedWakeWord(p_sdk, &result);
      // printf("I'm here! [C:%8.3f%%]\t", result.confidence * 100);
      char sbuf[1024];
      obj.set("status", "FLUENTAI_STATE_WAKE_WORD_DETECTED");
      if (result.command_id >= 0) {
        snprintf(sbuf, sizeof(sbuf) - 1, "FLUENTAI: Direct command: %d, %s\r\n",
                 result.command_id, result.word_string);
        obj.set("msg", sbuf);
        obj.set("command_id", result.command_id);
        obj.set("word_string",result.word_string);
        EM_ASM({
          fluentai_msg = Emval.toValue($0);
        }, obj.as_handle());
      } 
      else {
        snprintf(sbuf, sizeof(sbuf) - 1, "I'm here! [C:%8.3f%%]\t",
                 result.confidence * 100);
        obj.set("msg", sbuf);
        EM_ASM({
          fluentai_msg = Emval.toValue($0);
        }, obj.as_handle());
      }
      break;
    }
    case FLUENTAI_STATE_COMMAND_DETECTED: {
      char action_str[FLUENTAI_WORD_STRING_LENGTH];
      char object_str[FLUENTAI_WORD_STRING_LENGTH];
      fluentai_detection_result result;
      fluentai_getDetectedCommand(p_sdk, &result);
      parse_command(result.word_string, action_str, object_str);
      char sbuf[1024];
      snprintf(sbuf, sizeof(sbuf) - 1,
               "Execute : (action:%s,object:%s,C:%8.3f%%)\r\n", action_str,
               object_str, result.confidence * 100);
        obj.set("status", "FLUENTAI_STATE_COMMAND_DETECTED");
        obj.set("msg", sbuf);
        obj.set("action", action_str);
        obj.set("object", object_str);
        EM_ASM({
          fluentai_msg = Emval.toValue($0);
        }, obj.as_handle());
      // count_nb = 0;
      break;
    }
    case FLUENTAI_STATE_COMMAND_NOT_DETECTED:
      char sbuf[1024];
      snprintf(sbuf, sizeof(sbuf) - 1, "Command was not recognized!\r\n");
        obj.set("status", "FLUENTAI_STATE_COMMAND_NOT_DETECTED");
        obj.set("msg", sbuf);
        EM_ASM({
          fluentai_msg = Emval.toValue($0);
        }, obj.as_handle());
      waiting_for_command = false;
      // count_nb = 0;
      break;
    default: {
      const char *p_message = fluentai_getLastErrorMessage();
      printf("\nUnsupported status : %d\r\n", status);
      printf("Last error processing audio : %s\r\n", p_message);
      break;
    }
    } // end switch
  }
private:
  void setup() {
    p_sdk = fluentai_createInstance();
    const char *p_sdk_version =
        fluentai_getValue_string(p_sdk, "sdk_version", NULL);
    const char *p_model_name =
        fluentai_getValue_string(p_sdk, "model_name", NULL);
    int32_t sample_rate = fluentai_getValue_int(p_sdk, "sample_rate", NULL);
    char sbuf[1024];
    val obj = val::object();
    snprintf(sbuf , sizeof(sbuf) - 1, "Loading SDK: %s : MODEL: %s<br/>Starting audio with requested sample rate %d Hz",
             p_sdk_version, p_model_name, sample_rate);
        obj.set("status", "FLUENTAI_STATE_WAITING");
        obj.set("msg", sbuf);
        EM_ASM({
          fluentai_msg = Emval.toValue($0);
        }, obj.as_handle());
  }
  void *p_sdk{nullptr};
  uint8_t status{FLUENTAI_STATE_WAITING};
  uint8_t prev_status{FLUENTAI_STATE_WAITING};
  bool waiting_for_command{false};

};

EMSCRIPTEN_BINDINGS(CLASS_FluentaiKernel) {
  class_<FluentaiKernel>("FluentaiKernel")
      .constructor()
      .function("process", &FluentaiKernel::process, allow_raw_pointers());
}
