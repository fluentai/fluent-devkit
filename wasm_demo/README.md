# Overview
The Fluent.AI WebAssembly Demo is a browser-based solution that utilizes WebAssembly technology to implement speech recognition functionality. It is compatible with Windows, Linux, and macOS operating systems, and can be run on any web browser that supports WebAssembly, such as Google Chrome or Mozilla Firefox.

To build the project, users can clone the repository and run the provided Makefile.

It is important to note that when testing the demo on tablets and phones, microphone (front-end) issues have been reported. Therefore, the demo is currently recommended for laptop and desktop use.

# How to Build and Run this Demo
## Sync the Compiler Toolchains from the Build Server
To sync the necessary compiler toolchains from the build server, use the following command:
```
rsync --delete  -Pavur 129.153.40.75:/data/micro/emsdk/  /data/micro/emsdk/
```
## Build the WebAssembly Project
To build the WebAssembly project, follow these steps:

1. Clone the repository: 

```
git clone git@gitlab.com:fluentai/fluent-devkit.git
```

2. Navigate to the project directory:

```
cd fluent-devkit/wasm_demo
```

3. Unzip the model:

```
make unzip_model
```

4. Build the project:

```
make all
```
## Run the Demo on Your Local Machine
- To run the demo on your local machine, use the following command:
```
make run
```
- Please visit the weblink  http://localhost:8000/ using your web browser  


# Construct the WASM environment from scrach 
Generally, you don't have to do the following step unless you cannot  copy the neccessary toochain "/data/micro/emsdk" from the build server.

To compile this C++ code to WebAssembly, Please install Emscripten by following the instructions on [their website: ](https://emscripten.org/docs/getting_started/downloads.html)

# FYI
- [Embedding JavaScript snippets in C++ with Emscripten](https://web.dev/emscripten-embedding-js-snippets/)
