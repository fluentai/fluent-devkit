DIR_MODELS_ROOT?=$(PWD)/100_models
ifeq ($(PROJECT_NAME), ces_tws)
NUM_FEATURES=40
unzip_model : $(DIR_MODELS_ROOT)/TWS-py-20230118.zip $(DIR_MODELS_ROOT)/air_model_earbud-Luis-20230118.pth
	rm -rf  $(DIR_PROJECT)
	mkdir -p  $(DIR_PROJECT)
	unzip  -d $(DIR_PROJECT) $<
	mv $(DIR_PROJECT)/py $(DIR_PROJECT)/model_all
	cd $(DIR_PROJECT)/model_all && ln -sf  model_r2net_24mar.pth ww_model.pth

	#cd $(DIR_PROJECT)/model_all && ln -sf  model.pth air_model.pth
	cp -vf  $(DIR_MODELS_ROOT)/air_model_earbud-Luis-20230118.pth $(DIR_PROJECT)/model_all/
	cd $(DIR_PROJECT)/model_all && ln -sf  air_model_earbud-Luis-20230118.pth air_model.pth
	python3  $(DIR_MODELS_ROOT)/add_default_values_in_config_sdk.py  $(DIR_PROJECT)/model_all/config.sdk.json
else ifeq ($(PROJECT_NAME), TWS_small_Mnet_Nano)
NUM_FEATURES=40
unzip_model : $(DIR_MODELS_ROOT)/TWS_small_Mnet_Nano.zip
	rm -rf  $(DIR_PROJECT)
	mkdir -p  $(DIR_PROJECT)
	unzip  -d $(DIR_PROJECT) $<
	mv $(DIR_PROJECT)/py $(DIR_PROJECT)/model_all
	cd $(DIR_PROJECT)/model_all && patch -p3 -i $(PWD)/model_TWS_small_Mnet_Nano.patch
	python3  $(DIR_MODELS_ROOT)/add_default_values_in_config_sdk.py  $(DIR_PROJECT)/model_all/config.sdk.json
else ifeq ($(PROJECT_NAME), washer)
NUM_FEATURES=40
unzip_model : $(DIR_MODELS_ROOT)/washer_models.tgz
	rm -rf  $(DIR_PROJECT)
	mkdir -p  $(DIR_PROJECT)
	tar -zxvf $< -C $(DIR_PROJECT)
	python3  $(DIR_MODELS_ROOT)/add_default_values_in_config_sdk.py  $(DIR_PROJECT)/model_all/config.sdk.json
endif

  

