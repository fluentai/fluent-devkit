G?=native
ifeq ($(G), native)
#PLATFORM_CONVERT=none
PLATFORM=native
PLATFORM_CONVERT=arm
PLATFORM_ARCH=Linux_x86_64
LIB_POSTFIX=so
DEBUG_OPTS=#--debug
BUILD_DEBUG_OPTS=#--debug
else ifeq ($(G), native_log)
#PLATFORM_CONVERT=none
PLATFORM=native
PLATFORM_CONVERT=arm
PLATFORM_ARCH=Linux_x86_64
LIB_POSTFIX=so
DEBUG_OPTS=--debug
BUILD_DEBUG_OPTS=--debug
LOG_CALLBACK=1
else ifeq ($(G), native_debug)
#PLATFORM_CONVERT=none
PLATFORM=native
PLATFORM_CONVERT=arm
PLATFORM_ARCH=Linux_x86_64
LIB_POSTFIX=so
DEBUG_OPTS=--debug
BUILD_DEBUG_OPTS=--debug
else ifeq ($(G), hifi5_log)
DEBUG_OPTS=--debug
BUILD_DEBUG_OPTS=

PLATFORM=hifi5
PLATFORM_CONVERT=hifi5
PLATFORM_ARCH=$(PLATFORM)
LIB_POSTFIX=a
else ifeq ($(G), hifi4_log)
DEBUG_OPTS=--debug
BUILD_DEBUG_OPTS=

PLATFORM=hifi4
PLATFORM_CONVERT=hifi4
PLATFORM_ARCH=$(PLATFORM)
LIB_POSTFIX=a
else ifeq ($(G), dspg-d7)
DEBUG_OPTS=
BUILD_DEBUG_OPTS=

PLATFORM=dspg-d7
PLATFORM_CONVERT=none
PLATFORM_ARCH=$(PLATFORM)
LIB_POSTFIX=a
else ifeq ($(G), native_java)
DEBUG_OPTS=
BUILD_DEBUG_OPTS=

PLATFORM=native_java
PLATFORM_CONVERT=none
PLATFORM_ARCH=$(PLATFORM)
LIB_POSTFIX=a
else ifeq ($(G), wasm)
DEBUG_OPTS=
BUILD_DEBUG_OPTS=

PLATFORM=wasm
PLATFORM_CONVERT=arm
PLATFORM_ARCH=$(PLATFORM)
LIB_POSTFIX=a
else
PLATFORM=$(G)
PLATFORM_CONVERT=$(PLATFORM)
PLATFORM_ARCH=$(PLATFORM)
LIB_POSTFIX=a
DEBUG_OPTS=
BUILD_DEBUG_OPTS=
endif

LOG_CALLBACK?=0

APP_PATH?=./demo
PATH_AUDIO_FILE?=

DIR_ROOT_FLUENTAI?=/data/${USER}
DIR_FLUENTAI_SOURCE=$(DIR_ROOT_FLUENTAI)/source

DIR_FLUENTAI_MICRO?=$(DIR_FLUENTAI_SOURCE)/fluentai-micro.$(PLATFORM)
DIR_PROJECT_PARENT?=$(DIR_ROOT_FLUENTAI)/projects
DIR_PROJECT?=$(DIR_PROJECT_PARENT)/$(PROJECT_NAME)

PWD=$(shell pwd)

all: $(APP_PATH)
	@echo done $^

$(PATH_AUDIO_FILE):$(WAV_FILE) #post_audio_file.c
	#xxd -n audio_file  -i $< $@
	python3 $(DIR_FLUENTAI_MICRO)/scripts/wav_writer.py $< $@
ifeq ($(PLATFORM), native)
	#cat  post_audio_file.c >> $@
endif
	touch $@

build $(DIR_PROJECT)/sdk_$(PLATFORM)/$(PLATFORM_ARCH)/include/fluent/fluentai-sdk.h: $(DIR_PROJECT)/sdk_$(PLATFORM)/$(PLATFORM_ARCH)/lib/libfluentai-micro.$(LIB_POSTFIX)
	echo $@ generated with $< together

	#JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64 python $(DIR_FLUENTAI_SOURCE)/fluentai-tools/build_scripts/build.py 
$(DIR_PROJECT)/sdk_$(PLATFORM)/$(PLATFORM_ARCH)/lib/libfluentai-micro.$(LIB_POSTFIX): $(DIR_PROJECT)/q7_$(PLATFORM)/fluent/model/model_ww.hpp \
	$(DIR_PROJECT)/q7_$(PLATFORM)/fluent/model/model_ww.cpp \
	$(DIR_FLUENTAI_SOURCE)/fluentai-tools/build_scripts/build.py
	python $(DIR_FLUENTAI_SOURCE)/fluentai-tools/build_scripts/build.py \
		$(DIR_PROJECT)/q7_$(PLATFORM)  $(DIR_PROJECT)/sdk_$(PLATFORM) --micro $(DIR_FLUENTAI_MICRO) \
		--platform $(PLATFORM) --version $(PLATFORM) --toolchain_dir /data/micro \
		--build_path ./tmp/$(PROJECT_NAME)_$(PLATFORM)   --no_check_commit --no_question $(BUILD_DEBUG_OPTS)

#--ww ${wd}/py/${ww_model} 
#--intent_activation 'none'  
#--ww_features ${wd}/py/${features}  

$(DIR_PROJECT)/q7_$(PLATFORM)/fluent/model/model_ww.cpp: $(DIR_PROJECT)/q7_$(PLATFORM)/fluent/model/model_ww.hpp

$(DIR_PROJECT)/q7_$(PLATFORM)/fluent/model/model_ww.hpp : $(DIR_PROJECT)/model_all/labels.csv $(DIR_PROJECT)/model_all/config.sdk.json \
	$(DIR_FLUENTAI_MICRO)/scripts/model_converter.py
	python $(DIR_FLUENTAI_MICRO)/scripts/model_converter.py -f q7 \
		-o $(DIR_PROJECT)/q7_$(PLATFORM)  --platform $(PLATFORM_CONVERT) --config $(DIR_PROJECT)/model_all/config.sdk.json \
		--labels $(DIR_PROJECT)/model_all/labels.csv \
		--slots $(DIR_PROJECT)/model_all/slots.npy --ww $(DIR_PROJECT)/model_all/ww_model.pth \
		--intent $(DIR_PROJECT)/model_all/air_model.pth --num_features $(NUM_FEATURES) $(DEBUG_OPTS)

REPO_FLUENTAI_MICRO_CORE?=https://gitlab.com/fluentai/fluentai-micro.git
REPO_FLUENTAI_TOOLS?=https://gitlab.com/fluentai/fluentai-tools.git 

REPO_FLUENTAI_MICRO_CORE?=git@gitlab.com:fluentai/fluentai-micro.git
REPO_FLUENTAI_TOOLS?=git@gitlab.com:fluentai/fluentai-tools.git 

BRANCH_MICRO_CORE?=master
BRANCH_TOOLS?=master
$(DIR_FLUENTAI_SOURCE)/fluentai-tools/build_scripts/build.py:
	git clone  --branch $(BRANCH_TOOLS)  $(REPO_FLUENTAI_TOOLS) $(DIR_FLUENTAI_SOURCE)/fluentai-tools
$(DIR_FLUENTAI_MICRO)/scripts/model_converter.py:
	git clone  --branch $(BRANCH_MICRO_CORE)  $(REPO_FLUENTAI_MICRO_CORE)  $(DIR_FLUENTAI_MICRO)


clean_model: clean_build
	rm -fr $(DIR_PROJECT)/q7_$(PLATFORM)

clean_build:clean_app
	rm -fr $(DIR_PROJECT)/sdk_$(PLATFORM) $(DIR_FLUENTAI_MICRO)/tmp/$(PROJECT_NAME)_$(PLATFORM)


.PHONY: clean_model clean_build clean_app clean_all


ifeq ($(G), native_logxxx)
clean_app:
	make clean -C d_sdk_linux_demo
run:
	make $@ -C d_sdk_linux_demo
$(APP_PATH): $(APP_PATH).cpp $(DIR_PROJECT)/sdk_$(PLATFORM)/$(PLATFORM_ARCH)/lib/libfluentai-micro.$(LIB_POSTFIX) $(PATH_AUDIO_FILE)
	echo $^
	make all -C d_sdk_linux_demo
else ifeq ($(G),$(filter native native_log native_debug, $(G)))
run: $(APP_PATH)
	./$< $(WAV_FILE)
$(APP_PATH): $(APP_PATH).cpp $(DIR_PROJECT)/sdk_$(PLATFORM)/$(PLATFORM_ARCH)/lib/libfluentai-micro.$(LIB_POSTFIX) $(PATH_AUDIO_FILE)
	bear -- clang++ -g -o $@ $< -lfluentai-micro \
		-Wl,-rpath,$(DIR_PROJECT)/sdk_$(PLATFORM)/$(PLATFORM_ARCH)/lib \
		-L $(DIR_PROJECT)/sdk_$(PLATFORM)/$(PLATFORM_ARCH)/lib  -I$(DIR_PROJECT)/sdk_$(PLATFORM)/$(PLATFORM_ARCH)/include \
		-I$(DIR_FLUENTAI_MICRO)/include -D__LOG_CALLBACK__=$(LOG_CALLBACK)
	g++ -g -o $@ $< -lfluentai-micro \
		-Wl,-rpath,$(DIR_PROJECT)/sdk_$(PLATFORM)/$(PLATFORM_ARCH)/lib \
		-L $(DIR_PROJECT)/sdk_$(PLATFORM)/$(PLATFORM_ARCH)/lib  -I$(DIR_PROJECT)/sdk_$(PLATFORM)/$(PLATFORM_ARCH)/include \
		-I$(DIR_FLUENTAI_MICRO)/include -D__LOG_CALLBACK__=$(LOG_CALLBACK) -pg
endif

