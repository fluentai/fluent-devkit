#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 24 20:57:22 2020

@author: sam
"""
import os
import numpy as np
import pandas as pd
from scipy.io import wavfile
import argparse
import json
import tqdm

from fluent_sdk import FluentSDK


def readfile(filename, sdk_rate=None, channel=0):
    rate,data = wavfile.read(filename)
    if rate!=sdk_rate:
        raise Exception("Input file rate %d does not match expected sample rate %d"%(rate, sdk_rate))
    if len(data.shape)==2:
        data = data[:,channel].copy()
    if data.dtype==np.int32:
        data = np.round(data/65536).clip(-32768,32767).astype(np.int16).copy()
    return data


class Result:
    def __init__(self):
        self.cols = ['file_idx','ww_time','decoded_ww','ww_confidence','intent_time','decoded_intent','intent_confidence']
        self.clear()

    def clear(self):
        self.file_idx = -1
        self.ww = ''
        self.ww_confidence = 0
        self.ww_samples = 0
        self.ww_time = 0
        self.intent = ''
        self.intent_confidence = 0
        self.intent_samples = 0
        self.intent_time = 0
        self.intent_duration = 0

    def get(self):
        return [np.int32(self.file_idx), self.ww_time, self.ww, np.float32(self.ww_confidence), self.intent_time, self.intent, np.float32(self.intent_confidence)]

class FileSummary:
    
    def __init__(self):
        self.clear()
        self.cols = ['file_idx','duration','ww_count','intent_count', 'decoded_ww', 'decoded_intent','last_ww_time','last_intent_time','num_frames']
    
    def clear(self):
        self.file_idx = 0
        self.duration = 0
        self.ww_detections = 0
        self.intent_detections = 0
        self.ww=''
        self.intent=''
        self.last_ww_time = 0
        self.last_intent_time = 0
        self.num_frames = 0
        
    def get(self):
        return [np.int32(self.file_idx), np.float32(self.duration), np.int32(self.ww_detections), np.int32(self.intent_detections), self.ww, self.intent, self.last_ww_time, self.last_intent_time, self.num_frames]

class Tester:
    def __init__(self, sdk, audio_out_path):
        self.sdk = sdk
        self.sample_rate = self.sdk.getValueInt("sample_rate")
        self.audio_out_path = audio_out_path
        self.verbose = False
        self.chunk_size = 640
        self.ww_duration = 1.2
        self.append_silence_enabled = True
        self.waiting_for_intent = False
        self.file_summary = FileSummary()
        self.channel = 0

    
    def get_chunks(self, data, progressbar):
        for i in range(0,len(data)-self.chunk_size, self.chunk_size):
            yield data[i:i+self.chunk_size]
            progressbar.update(self.chunk_size/self.sample_rate)

        leftover_count = len(data)%self.chunk_size
        if leftover_count>0:
            leftover = np.zeros(self.chunk_size, np.int16)
            leftover[:leftover_count] = data[len(data)-leftover_count:]
            yield leftover
            progressbar.update(leftover_count/self.sample_rate)

        if self.append_silence_enabled:
            zeros = np.zeros(self.chunk_size, np.int16)
            i = 0
            while not self.waiting_for_intent and i<self.sample_rate//2:
                yield zeros
                i += self.chunk_size

            i = 0
            while i<self.sample_rate*5 and self.waiting_for_intent:
                yield zeros
                i += self.chunk_size

    def process_file(self, data, file_idx, progressbar):
        samples = 0
        status = 0
        self.waiting_for_intent = False

        result = Result()
        self.file_summary.clear()
        self.file_summary.num_frames = 0
        
        for chunk in self.get_chunks(data, progressbar):
            do_output = False
            samples += len(chunk)
            status = self.sdk.processAudio(chunk)
            self.file_summary.num_frames += 1
            ww_detected = status==FluentSDK.WAKE_WORD_DETECTED
            intent_detected = status==FluentSDK.COMMAND_DETECTED
            
            if ww_detected:
                result.ww, result.ww_confidence = self.sdk.getDetectedWakeword()
                result.ww_samples = samples
                self.waiting_for_intent = True
                self.file_summary.ww_detections += 1
                self.file_summary.ww = result.ww
                self.file_summary.last_ww_time = samples/self.sample_rate
                
            if intent_detected:
                result.intent, result.intent_confidence = self.sdk.getDetectedCommand()
                result.intent_samples = samples
                result.intent_duration = self.sdk.getValueInt("command_duration_ms")*self.sample_rate//1000
                do_output = True
                self.file_summary.intent_detections += 1
                self.file_summary.intent = result.intent
                self.file_summary.last_intent_time = samples/self.sample_rate
            
            if self.waiting_for_intent and not status in (FluentSDK.RECORDING_COMMAND, FluentSDK.WAKE_WORD_DETECTED):
                do_output = True
            
            if do_output:
                result.file_idx = file_idx
                result.ww_time = np.float32(result.ww_samples/self.sample_rate)
                result.intent_time = np.float32(result.intent_samples/self.sample_rate)
                yield result
                result.clear()
                self.waiting_for_intent = False

    def process_files(self,df, output_file):
        filenames = df.filename.values
        if 'input_offset' in df.columns:
            is_archive = True
            offset = df.input_offset.values
            count = df.input_count.values
        else:
            is_archive = False
    
        curfile = None
        data = None
        writer = CsvWriter(output_file, Result().cols, 10)
        summary_writer = CsvWriter(None, FileSummary().cols )
        total_data_size = 0
        num_files = len(filenames)
        progressbar = tqdm.tqdm(total=num_files)
        for file_idx,filename in enumerate(filenames):
            if filename != curfile:
                curfile = filename
                if is_archive:
                    print("Reading file ",curfile)
                data = readfile(curfile, self.sample_rate, self.channel)
                    
            self.sdk.reset()
            if is_archive and count[file_idx]>=0:
                start_idx = offset[file_idx]
                end_idx = start_idx + count[file_idx]
                utterance = data[start_idx: end_idx]
            else:
                utterance = data
            total_data_size += len(utterance)
            estimated_total = int(total_data_size * num_files / (file_idx+1) / self.sample_rate)
            progressbar.total = estimated_total
    
            for result in self.process_file(utterance, file_idx, progressbar):
                writer.add_row(result.get())
                if self.audio_out_path:
                    line_idx = len(writer.rows)-1
                    if result.ww:
                        start_sample = max(0, result.ww_samples - int(self.ww_duration*self.sample_rate)) 
                        clip = utterance[start_sample:result.ww_samples]
                        audio_out_file = os.path.join(self.audio_out_path, "wake_%04d.wav"%line_idx)
                        wavfile.write(audio_out_file, self.sample_rate, clip)
                    if result.intent:
                        
                        start_sample = max(0, result.intent_samples - int(result.intent_duration))
                        clip = utterance[start_sample:result.intent_samples]
                        audio_out_file = os.path.join(self.audio_out_path, "intent_%04d.wav"%line_idx)
                        wavfile.write(audio_out_file, self.sample_rate, clip)
            
            self.file_summary.file_idx = file_idx
            self.file_summary.duration = len(utterance)/self.sample_rate
            summary_writer.add_row(self.file_summary.get())
            file_idx += 1
    
        output_df = writer.get_df()
        summary_df = summary_writer.get_df()
        return output_df, summary_df

class CsvWriter:
    def __init__(self, filename, cols, write_interval=0):
        self.filename = filename
        self.cols = cols
        self.rows = []
        self.new_rows = 0
        self.write_interval = write_interval
    
    def add_row(self, row):
        self.rows.append(row)
        self.new_rows += 1
        if self.write_interval>0 and self.new_rows>=self.write_interval:
            self.flush()
    
    def flush(self):
        if self.new_rows==0:
            return
        if self.new_rows == len(self.rows):
            df = pd.DataFrame(self.rows, columns=self.cols)
            df.to_csv(self.filename, index=False)
        else:
            df = pd.DataFrame(self.rows[-self.new_rows:], columns=self.cols)
            df.to_csv(self.filename, index=False, mode='a', header=False)
        self.new_rows = 0
    
    def get_df(self):
        if self.write_interval>0:
            self.flush()
        df = pd.DataFrame(self.rows, columns=self.cols)
        return df

def read_input(filename, audio_root_path):
    if filename.endswith('.wav'):
        return pd.DataFrame([[filename]], columns=['filename'])
    elif filename.endswith('.txt'):
        with open(filename,'r') as f:
            wavfiles = [s.strip() for s in f.read().split('\n')]
        wavfiles = [i for i in wavfiles if i.endswith('.wav')]
        df = pd.DataFrame()
        df['filename'] = wavfiles
        if audio_root_path is None:
            audio_root_path = os.path.dirname(filename)
        df['filename'] = df.filename.apply(lambda x:os.path.join(audio_root_path, x))
        return df

    elif filename.endswith('.csv'):
        df = pd.read_csv(filename)
        # Convert column names to lowercase
        df.rename(columns={i:i.lower() for i in df.columns}, inplace=True)
        
        if 'archive_file' in df.columns:
            df.rename(columns={'filename':'original_file','archive_file':'filename'}, inplace=True)
            
        to_rename = [('offset','input_offset'), ('count','input_count'), ('command','intent')]
        for k,v in to_rename:
            if k in df.columns and not v in df.columns:
                df.rename(columns={k:v}, inplace=True)
                
        to_remove = set(Result().cols)&set(df.columns)
        df.drop(to_remove, 1, inplace=True)

        if audio_root_path is None:
            audio_root_path = os.path.dirname(filename)
        df['filename'] = df.filename.apply(lambda x:os.path.join(audio_root_path, x))
        
        return df
    else:
        return None

        
def read_config(sdk, config_file):
    with open(config_file,'r') as f:
        config = json.load(f)
    for k,v in config.items():
        if type(v) is float:
            sdk.setValueFloat(k,v)
        elif type(v) is int:
            sdk.setValueInt(k,v)
        else:
            print("Unsupported type! ",k,type(v),v)


def analyze(df, groupby=None):
    print("Analyzing")
    df['decoded_intent'] = df.decoded_intent.fillna('')
    df['intent'] = df.intent.fillna('')
    df['correct'] = df.decoded_intent == df.intent
    oov_df = df[(df.intent=='')|(df.intent==0)]
    iv_df = df[(df.intent!='')&(df.intent!=0)].copy()
    if len(oov_df)==0:
        ww_fahr = 0
        fahr = 0
    else:
        ww_fahr = 3600*oov_df.ww_count.sum()/oov_df.duration.sum()
        fahr = 3600*oov_df.intent_count.sum()/oov_df.duration.sum()
    
    iv_df['ww_fr'] = iv_df.ww_count==0
    iv_df['missing'] = iv_df.intent_count==0
    iv_df['misclassified'] = (iv_df.intent_count>0)&(iv_df.correct==0)
    iv_df['fr'] = (iv_df.intent_count==0)|(iv_df.correct==0)
    if len(iv_df)==0:
        ww_frr = np.nan
        frr = np.nan
        misclassified = np.nan
        undetected = np.nan
    else:
        ww_frr = iv_df.ww_fr.mean()
        undetected = iv_df.missing.mean()
        misclassified = iv_df.misclassified.mean()
        frr = iv_df.fr.mean()
    print("WW FA/day = %.2f/day"%(ww_fahr*24))
    print("WW FRR = %.2f%%"%(100*ww_frr))
    print("Intent FA/day = %.2f/day"%(fahr*24))
    print("Intent FRR = %.2f%%   -  %.2f%% missing, %.2f%% misclassified"%(100*frr, 100*undetected, 100*misclassified))
    
    if not groupby is None:
        print("\nPer group intent FRR:")
        summary = iv_df.groupby(groupby)[['fr','missing','misclassified']].mean()
        pd.options.display.float_format = '{:,.2f}%'.format
        print(summary*100)
        

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('sdk', help='SDK library file  (libfluentai-micro.so)')
    parser.add_argument('input', help='Input file.  One of:\n\t.wav file\n\t.txt file containing list of .wav files, or\n\t.csv file - "filename" column isrequired. "input_offset" and "input_count" columns optional to test a region of the file')
    parser.add_argument('output', help='Output file (.csv)')

    parser.add_argument('--audioout', help='Path where detected wakeword/intent audios will be output.  Output format will be path/wake_0000.wav and path/intent_0000.wav')
    parser.add_argument('--config', help='Setup model paramters from this config.json file')
    parser.add_argument('--wavroot', help='Root path for wav files that have relative paths.  By default use input path')
    parser.add_argument('--channel', help='Select which channel of the audio file to use.  By default uses first channel', default=0, type=int)
    parser.add_argument('--groupby', help='When analyzing results, group by these columns (comma separated list)')
    
    analyze_only = False
    args = parser.parse_args()
    
    sdk = FluentSDK(args.sdk)
    sdk.channel = args.channel
    
    input_df = read_input(args.input, args.wavroot)
    if input_df is None:
        with sdk:
            version = sdk.getValueString("sdk_version")
            model = sdk.getValueString("model_name")
            print("SDK Version",version,"Model",model)
        return

    filename,ext = os.path.splitext(args.output)
    summary_file = filename+"_perfile"+ext

    if analyze_only:
        summary_df = pd.read_csv(summary_file)
    else:
        with sdk:
            version = sdk.getValueString("sdk_version")
            model = sdk.getValueString("model_name")
            print("SDK Version",version,"Model",model)
            if args.config:
                read_config(sdk, args.config)
            tester = Tester(sdk, args.audioout)
            output_df, summary_df = tester.process_files(input_df, args.output)
    
        cols = [i for i in input_df.columns if not i in summary_df.columns]
        summary_df = pd.concat([input_df[cols], summary_df], axis=1)
        summary_df.to_csv(summary_file, index=False)
        
    if 'intent' in input_df:
        groupby=None
        if args.groupby:
            groupby = args.groupby.split(',')
        analyze(summary_df, groupby=groupby)
    

if __name__ == "__main__":
    main()
