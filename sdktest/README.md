# sdktest
This is a utility to evaluate Fluent's SDK offline on prerecorded .wav files

## Requirements
Linux-based OS
Fluent SDK
Python 3.*

Python packages:
numpy
scipy
pandas
tqdm


## Usage
```
python sdktest.py SDK INPUT OUTPUT [--audioout AUDIOPATH] [--config CONFIG] 
```
e.g.
```
python sdktest.py libfluentai-micro.so data.csv results.csv --audioout /my/audio/out/path/ --config config.json --train train.csv
```
* SDK - This is the library .so file that will be used for decoding
* INPUT - This is one of
	* .wav file
	* .txt file containing a list of .wav files
	* .csv file contining the following columns
		* filename   - .wav file
		* input_offset   - [Optional] This is used if you want to use treat a part of the file as a separate utterance (e.g. if the file contains multiple utterances).  This is the start of the utterance in the file in samples.  If not specified, the entire .wav file is considered to be one utterance.
		* input_count   - [Optional] Number of samples in the utterance.
		* intent   - [Optional] The intent spoken in this utterance.  Use '' for OOV.  Alternatively, you can use values 0/1 for OOV/IV
		* truth - Same as intent

* OUTPUT - This is a .csv file where results will be output.  A second file will be output as well containing a list of results per file.  The filename for the second file will be OUTPUT, but with "_perfile" appended to the end

* AUDIOPATH - A folder.  When this is specified, detected wakeword and intents will be cut out and saved in this folder.  The fileformat is wake_XXXX.wav and intent_XXXX.wav, where XXXX is an ID corresponding to the line number in the output .csv file.

* CONFIG - A json file.  This can be used to configure any configurable parameters of the SDK.  Format should be {"key":value, "key2":value2}  Some supported keys are:
    * early_stop_repetitions
    * ww_threshold
    * command_threshold
    * early_stop_threshold
    

## Results
The result .csv contains the following columns:

* file_idx - Row # of the file in input.csv
* ww_time - time in seconds from the start of the utterance where the wakeword was detected
* ww - Detected wakeword
* ww_confidence - WW Confidence
* intent_time - time in seconds from the start of the utterance where the intent was detected
* intent - Detected intent, or blank if not
* intent_confidence - Intent confidence

results_perfile.csv contains the following columns:

* file_idx - Row # of the file in input.csv
* ww_count - Number of wakewords detected in this file
* intent_count - Number of intents detected in this file
* max_ww_confidence - Maximum WW confidence detected in this file
* max_intent_confidence - Maximum intent confidence detected in this file
* intent - The last intent detected in this file
