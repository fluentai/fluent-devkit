import ctypes
import numpy as np

class DetectionResult(ctypes.Structure):
    _fields_=[("word_string",ctypes.c_char * 256),
              ("confidence",ctypes.c_float)]

class FluentSDK:
    WAITING = 0
    VOICE_DETECTED = 1
    WAKE_WORD_DETECTED = 2
    RECORDING_COMMAND = 3
    COMMAND_DETECTED = 4
    COMMAND_NOT_DETECTED = 5
    ERROR = 0xFF
    
    def __init__(self, library_path):
        self.sdk = ctypes.CDLL(library_path)

        self.sdk.fluentai_createInstance.restype = ctypes.c_void_p    

        self.sdk.fluentai_deleteInstance.argtypes = (ctypes.POINTER(ctypes.c_void_p), )

        self.sdk.fluentai_processAudio.argtypes = (ctypes.c_void_p, ctypes.POINTER(ctypes.c_int16), ctypes.c_uint32)
        self.sdk.fluentai_processAudio.restype = ctypes.c_uint8

        self.sdk.fluentai_reset.argtypes = (ctypes.c_void_p, )

        self.sdk.fluentai_getDetectedWakeWord.argtypes = (ctypes.c_void_p, ctypes.POINTER(DetectionResult))
        self.sdk.fluentai_getDetectedCommand.argtypes = (ctypes.c_void_p, ctypes.POINTER(DetectionResult))
        self.sdk.fluentai_getDetectedSpeaker.argtypes = (ctypes.c_void_p, ctypes.POINTER(DetectionResult))

        self.sdk.fluentai_startEnrollment.argtypes = (ctypes.c_void_p, ctypes.c_char_p)
        self.sdk.fluentai_startEnrollment.restype = ctypes.c_uint8

        self.sdk.fluentai_enroll.argtypes = (ctypes.c_void_p, ctypes.POINTER(ctypes.c_int16), ctypes.c_uint32)
        self.sdk.fluentai_enroll.restype = ctypes.c_uint8

        self.sdk.fluentai_enrollmentDone.argtypes = (ctypes.c_void_p, )
        self.sdk.fluentai_enrollmentDone.restype = ctypes.c_uint8

        self.sdk.fluentai_resetModelState.argtypes = (ctypes.c_void_p, )

        self.sdk.fluentai_getValue_int.argtypes = (ctypes.c_void_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_uint8))
        self.sdk.fluentai_getValue_int.restype = ctypes.c_int32

        self.sdk.fluentai_getValue_float.argtypes = (ctypes.c_void_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_uint8))
        self.sdk.fluentai_getValue_float.restype = ctypes.c_float

        self.sdk.fluentai_getValue_string.argtypes = (ctypes.c_void_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_uint8))
        self.sdk.fluentai_getValue_string.restype = ctypes.c_char_p

        self.sdk.fluentai_setValue_int.argtypes = (ctypes.c_void_p, ctypes.c_char_p, ctypes.c_int32, ctypes.POINTER(ctypes.c_uint8))
        self.sdk.fluentai_setValue_float.argtypes = (ctypes.c_void_p, ctypes.c_char_p, ctypes.c_float, ctypes.POINTER(ctypes.c_uint8))

        self.sdk.fluentai_getLastErrorMessage.argtypes = (ctypes.c_void_p, )
        self.sdk.fluentai_getLastErrorMessage.restype = ctypes.c_char_p
        
    def __enter__(self):
        self.createInstance()
        return self

    def __exit__(self, type, value, traceback):
        self.deleteInstance()
    
    def _encode(self, s):
        if type(s) is bytes:
            return s
        else:
            return s.encode('utf8')

    def _decode(self, s):
        try:
            return s.decode('utf8')
        except UnicodeDecodeError:
            return s
    
    def createInstance(self):
        self.instance = self.sdk.fluentai_createInstance()
    
    def deleteInstance(self):
        if self.instance:
            self.sdk.fluentai_deleteInstance(ctypes.byref(ctypes.c_void_p(self.instance)))
        self.instance = None
    
    def processAudio(self, data):
        assert(self.sdk and self.instance)
        assert(data.dtype == np.int16)
        status = self.sdk.fluentai_processAudio(self.instance, data.ctypes.data_as(ctypes.POINTER(ctypes.c_int16)), ctypes.c_uint32(len(data)))
        return status

    def reset(self):
        self.sdk.fluentai_reset(self.instance)


    def _getDetected(self, fn):
        result = DetectionResult(b'', 0)
        fn(self.instance,ctypes.byref(result))
        word = self._decode(result.word_string)
        confidence = result.confidence
        return word,confidence

    def getDetectedWakeword(self):
        return self._getDetected(self.sdk.fluentai_getDetectedWakeWord)
    def getDetectedCommand(self):
        return self._getDetected(self.sdk.fluentai_getDetectedCommand)
    def getDetectedSpeaker(self):
        return self._getDetected(self.sdk.fluentai_getDetectedSpeaker)

    def startEnrollment(self, name):
        status = self.sdk.fluentai_startEnrollment(self.instance, self._encode(name))
        return status

    def enroll(self, data):
        assert(self.sdk and self.instance)
        assert(data.dtype == np.int16)
        status = self.sdk.fluentai_enroll(self.instance, data.ctypes.data_as(ctypes.POINTER(ctypes.c_int16)), ctypes.c_uint32(len(data)))
        return status

    def enrollmentDone(self):
        status = self.sdk.fluentai_enrollmentDone(self.instance)
        return status
    
    def resetModelState(self):
        self.sdk.fluentai_resetModelState(self.instance)

    def getValueInt(self, key):
        err = ctypes.c_uint8(0)
        value = self.sdk.fluentai_getValue_int(self.instance, self._encode(key), ctypes.byref(err))
        assert(err.value==0)
        return value

    def getValueFloat(self, key):
        err = ctypes.c_uint8(0)
        value = self.sdk.fluentai_getValue_float(self.instance, self._encode(key), ctypes.byref(err))
        assert(err.value==0)
        return value

    def getValueString(self, key):
        err = ctypes.c_uint8(0)
        s = self.sdk.fluentai_getValue_string(self.instance, self._encode(key), ctypes.byref(err))
        assert(err.value==0)
        return self._decode(s)

    def setValueInt(self, key, value):
        err = ctypes.c_uint8(0)
        self.sdk.fluentai_setValue_int(self.instance, self._encode(key), value, ctypes.byref(err))
        assert(err.value==0)

    def setValueFloat(self, key, value):
        err = ctypes.c_uint8(0)
        self.sdk.fluentai_setValue_float(self.instance, self._encode(key), value, ctypes.byref(err))
        assert(err.value==0)
    
    def getLastErrorMessage(self):
        s = self.sdk.fluentai_getLastErrorMessage(self.instance)
        return self._decode(s)
