cmake_minimum_required(VERSION 3.0.2)
project(sdk_windows_demo)
enable_language(CXX)
find_package(PkgConfig REQUIRED)

add_executable(sdk_windows_demo sdk_windows_demo.cpp audio.cpp)

target_include_directories(sdk_windows_demo PUBLIC "../include")
target_link_libraries(sdk_windows_demo PUBLIC -L"../lib" -lfluentai-micro -lportaudio)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wextra -Wall -std=c++11 -pedantic")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")

target_compile_options(sdk_windows_demo PUBLIC -std=c++11)

install(
	TARGETS sdk_windows_demo
	LIBRARY DESTINATION lib
	ARCHIVE DESTINATION lib
	RUNTIME DESTINATION bin
)
