// sdk_windows_demo.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

#include <fluent/fluentai-sdk.h>

#include <iostream>
#include <string>
#include <chrono>
#include <fstream>

#include <signal.h>
#include <portaudio.h>


// Demo version
#define SDK_DEMO_VERSION "1.0.2"

// Configuration
const int32_t PERIOD_SIZE = 160;
#define FRAMES_PER_BUFFER PERIOD_SIZE
const uint32_t MAX_QUEUE_SIZE = 100;

// Action used to g_stop the demo.
// Use none or an invalid action name for no g_stop action
#define DEMO_STOP_ACTION "none"

// Enable the following define to support multiple command mode
//#define ENABLE_MULTIPLE_COMMAND


// Allow to g_stop application nicely by catching Ctrl-C signal
bool g_stop = false;
void sigint_handler(int /*signal*/)
{
    printf("\n  Ctrl-C detected...\n");
    g_stop = true;
}

// Flag set when a wake word is detected and we are waiting for an intent/
bool g_waiting_for_intent = false;
// File opened for logging when required
std::ofstream g_logfile;
// SDK pointer
void* g_sdk = nullptr;

// This function list all audio devices found on the host.
void list_all_audio_devices(void)
{
    int numDevices = Pa_GetDeviceCount();
    PaDeviceIndex deviceIndex = Pa_GetDefaultInputDevice(); /* default input device */

    if (numDevices < 0)
    {
        printf("ERROR: PortAudio returned %d devices\n", numDevices);
    }
    else
    {
        const PaDeviceInfo* p_device_info;
        printf("List of the %d audio devices available on this host (including microphone and speaker)\n", numDevices);
        for (int i = 0; i < numDevices; i++)
        {
            p_device_info = Pa_GetDeviceInfo(i);
            printf("Device Name: %s index: %d hostApi %d", p_device_info->name, i, p_device_info->hostApi);
            if (deviceIndex == i)
            {
                printf(" (Default)\n");
            }
            else
            {
                printf("\n");
            }
        }
    }
}

void parse_command(const char *p_command, char *p_action, char *p_object)
{
    int str_index = 0;
    int substr_index = 0;
    enum parsing_state_e {
        PARSING_ACTION,
        PARSING_ACTION_FOUND,
        PARSING_OBJECT,
        PARSING_OBJECT_FOUND,
        PARSING_DONE
    } parsing_state = PARSING_ACTION;

    p_action[0] = '\0';
    p_object[0] = '\0';
    while (p_command[str_index] != '\0')
    {
        if (p_command[str_index] == '"')
        {
            // Skip these characters
            str_index++;
            continue;
        }

        switch (parsing_state)
        {
            case PARSING_ACTION:
                if (p_command[str_index] == ' ')
                {
                    // Skip these characters
                    str_index++;
                    continue;
                }
                else if (p_command[str_index] == ':')
                {
                    parsing_state = PARSING_ACTION_FOUND;
                    substr_index = 0;
                }
                break;
            case PARSING_ACTION_FOUND:
                if ((p_command[str_index] == ',') || (p_command[str_index] == '}'))
                {
                    parsing_state = PARSING_OBJECT;
                    p_action[substr_index] = '\0';
                }
                else
                {
                    p_action[substr_index++] = p_command[str_index];
                }
                break;
            case PARSING_OBJECT:
                if (p_command[str_index] == ' ')
                {
                    // Skip these characters
                    str_index++;
                    continue;
                }
                else if (p_command[str_index] == ':') {
                    parsing_state = PARSING_OBJECT_FOUND;
                    substr_index = 0;
                }
                break;
            case PARSING_OBJECT_FOUND:
                if (p_command[str_index] == '}')
                {
                    parsing_state = PARSING_DONE;
                    p_object[substr_index] = '\0';
                }
                else
                {
                    p_object[substr_index++] = p_command[str_index];
                }
                break;
            default:
                // Nothing to do
                break;
        } // end switch
        str_index++;
    } // end while
}

// This callback receives input audio stream and feed the SDK with it
static int captureCallback(
    const void* inputBuffer,
    void* outputBuffer,
    unsigned long framesPerBuffer,
    const PaStreamCallbackTimeInfo* timeInfo,
    PaStreamCallbackFlags statusFlags,
    void* userData)
{
    static bool waiting_for_command = false;

    if (inputBuffer == nullptr)
    {
        printf("Null input buffer\n");
        return paAbort;
    }

    try
    {
        static int prev_status = FLUENTAI_STATE_WAITING;
        // Detect wake phrase or post commands
        int status = fluentai_processAudio(g_sdk, (const int16_t*)inputBuffer, framesPerBuffer);

        if (status != prev_status)
        {
            switch (status)
            {
                case FLUENTAI_STATE_WAITING:
                    // Nothing to do waiting for wake word
                    break;

                case FLUENTAI_STATE_RECORDING_COMMAND:
                    // Nothing to do wake word detected recording command
                    break;

                case FLUENTAI_STATE_ERROR:
                {
                    std::string message = fluentai_getLastErrorMessage();
                    printf("\nError processing audio : %s\n", message.c_str()); fflush(stdout);
                    return paAbort;
                }

#ifdef ENABLE_MULTIPLE_COMMAND
                case FLUENTAI_STATE_COMMAND_COMPLETED:
                    // Nothing to do multiple commands were detected
                    waiting_for_command = false;
                    printf("Multiple commands completed\n"); fflush(stdout);
                    break;
#endif

                case FLUENTAI_STATE_WAKE_WORD_DETECTED:
                {
                    if (waiting_for_command)
                    {
                        if (g_logfile.is_open()) g_logfile << "\t\t\n";
                        printf("  Command was not completed!\n"); fflush(stdout);
                    }
                    fluentai_detection_result result;
                    fluentai_getDetectedWakeWord(g_sdk, &result);
                    auto timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
                    std::time_t time = std::time(nullptr);
                    std::string timestr = std::asctime(std::localtime(&time));
                    timestr.pop_back(); // remove final newline
                    // Do not echo result.word_string as it is always equal to "wakeword" when in STATE_WAKE_WORD_DETECTED
                    printf("%s WW Confidence %07.04f%% ", timestr.c_str(), result.confidence * 100); fflush(stdout);
                    if (g_logfile.is_open())
                    {
                        g_logfile << timestamp << '\t' << result.word_string << '\t' << result.confidence << '\t' << std::flush;
                    }
                    waiting_for_command = true;
                    break;
                }

                case FLUENTAI_STATE_COMMAND_DETECTED:
                {
                    char action_str[FLUENTAI_WORD_STRING_LENGTH];
                    char object_str[FLUENTAI_WORD_STRING_LENGTH];
                    auto timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
                    fluentai_detection_result result;
                    fluentai_getDetectedCommand(g_sdk, &result);
                    parse_command(result.word_string, action_str, object_str);
                    printf("AIR Confidence %07.04f%% : %s %s\n", result.confidence * 100, action_str, object_str); fflush(stdout);
                    if (g_logfile.is_open())
                    {
                        g_logfile << timestamp << '\t' << result.word_string << '\t' << result.confidence << '\n' << std::flush;
                    }
#ifndef ENABLE_MULTIPLE_COMMAND
                    waiting_for_command = false;
#endif

                    if (strstr(result.word_string, DEMO_STOP_ACTION) != nullptr)
                    {
                        // If the word stop is spoken then exit demo
                        printf("\n  Stop action detected...\n"); fflush(stdout);
                        return paAbort;
                    }
                    break;
                }

                case FLUENTAI_STATE_COMMAND_NOT_DETECTED:
                {
                    printf("Command was not recognized!\n"); fflush(stdout);
                    if (g_logfile.is_open()) g_logfile << "\t\t\n";
                    waiting_for_command = false;
                    break;
                }

                case FLUENTAI_STATE_COMMAND_COMPLETED:
                    /* Stop the command detection */
                    // Nothing to do multiple commands were detected
                    waiting_for_command = false;
                    printf("FLUENTAI: Multiple commands completed");
                    if (g_logfile.is_open()) g_logfile << "\t\t\n";
                    break;

                default:
                {
                    std::string message = fluentai_getLastErrorMessage();
                    printf("\nUnsupported status : %d\n", status);
                    printf("Last error processing audio : %s\n", message.c_str()); fflush(stdout);
                    return paAbort;
                }
            } // end switch
            prev_status = status;
        } // end if
    }
    catch (std::string& s)
    {
        printf("\nError while listening: %s\n", s.c_str()); fflush(stdout);
        return paAbort;
    }

    return paContinue;
}

void display_what_you_say(std::string device_name, std::string logfilename)
{
    // Setup Fluent SDK
    g_sdk = fluentai_createInstance();
    std::string sdk_version = fluentai_getValue_string(g_sdk, "sdk_version", nullptr);
    std::string model_name = fluentai_getValue_string(g_sdk, "model_name", nullptr);
    uint32_t sample_rate = fluentai_getValue_int(g_sdk, "sample_rate", nullptr);
    printf("  Loading SDK: %s : MODEL: %s\n", sdk_version.c_str(), model_name.c_str());
    printf("  Starting audio with requested sample rate %d Hz\n", sample_rate);

    // Optionally set confidence thresholds for WW and intent recognition. Otherwise, it will use the default values.
    // fluentai_setValue_float(g_sdk, "ww_threshold", 0.95, nullptr)
    // fluentai_setValue_float(g_sdk, "command_threshold", 0.5, nullptr)
#ifdef ENABLE_MULTIPLE_COMMAND
    // By default, multiple commands is disabled
    fluentai_setValue_int(g_sdk, "multiple_command", 1, nullptr);
#endif

    // Setup audio device
    PaStreamParameters  inputParameters;
    PaError err;

    /** A valid device index in the range 0 to (Pa_GetDeviceCount()-1)
        specifying the device to be used or the special constant
        paUseHostApiSpecificDeviceSpecification which indicates that the actual
        device(s) to use are specified in hostApiSpecificStreamInfo.
        This field must not be set to paNoDevice.
    */
    //PaDeviceIndex device;
    if (device_name.compare("default") == 0)
    {
        inputParameters.device = Pa_GetDefaultInputDevice(); /* default input device */
        if (inputParameters.device == paNoDevice)
        {
            printf("Error: PortAudio reports no default input device.\n"); fflush(stdout);
            goto done;
        }
    }
    else
    {
        inputParameters.device = std::atoi(device_name.c_str());
        if ((inputParameters.device < 0) || (inputParameters.device > (Pa_GetDeviceCount() - 1)))
        {
            printf("Error: Invalid index as audio device for PortAudio (%d).\n", inputParameters.device); fflush(stdout);
            goto done;
        }
    }
    inputParameters.channelCount = 1;                    /* stereo input */
    inputParameters.sampleFormat = paInt16;
    inputParameters.suggestedLatency = Pa_GetDeviceInfo(inputParameters.device)->defaultLowInputLatency;
    inputParameters.hostApiSpecificStreamInfo = NULL;

    // Allow clean g_stop using Ctrl-C
    signal(SIGINT, sigint_handler);

    PaStream* stream;
    /* Record some audio. -------------------------------------------- */
    err = Pa_OpenStream(
        &stream,
        &inputParameters,
        NULL,                  /* &outputParameters, */
        sample_rate,
        FRAMES_PER_BUFFER,
        paClipOff,      /* we won't output out of range samples so don't bother clipping them */
        captureCallback,
        nullptr);
    if (err != paNoError)
    {
        printf("Error: Failed to open stream with PortAudio %d.\n", err); fflush(stdout);
        goto done;
    }

    const PaStreamInfo* p_stream_info = Pa_GetStreamInfo(stream);
    printf("  Audio started with effective sample rate %Lf Hz\n", p_stream_info->sampleRate); fflush(stdout);

    if (!logfilename.empty())
    {
        g_logfile.open(logfilename);
        g_logfile << "ww_timestamp\tww\tww_confidence\tintent_timestamp\tintent\tintent_confidence" << std::endl;
    }

    err = Pa_StartStream(stream);
    if (err != paNoError) {
        printf("Error: Failed to starting stream with PortAudio %d.\n", err); fflush(stdout);
        goto done;
    }

    printf("\n=== Now running!! Please speak into the microphone. ===\n"); fflush(stdout);

    // Loop while streaming ends or Ctrl-C is received
    while (!g_stop && (Pa_IsStreamActive(stream) == 1))
    {
        Pa_Sleep(1000);
    }

done:
    // Cleaning
    err = Pa_IsStreamActive(stream);
    if (err != 1)
    {
        printf("Streaming stopped (%d)...\n", err); fflush(stdout);
    }
    else if (g_stop)
    {
        printf("Shutting down...\n"); fflush(stdout);
    }
    else
    {
        printf("Unknown condition for stopping...\n"); fflush(stdout);
    }

    if (g_logfile.is_open())
    {
        if (g_waiting_for_intent) g_logfile << "\t\t\n";
        g_logfile.close();
        g_waiting_for_intent = false;
    }

    err = Pa_CloseStream(stream);
    if (err != paNoError)
    {
        printf("Error: Failed to closing stream with PortAudio %d.\n", err); fflush(stdout);
    }
    fluentai_deleteInstance(&g_sdk);
}

int main(int argc, char* argv[])
{
    auto start_time = std::chrono::high_resolution_clock::now();
    if (argc > 1)
    {
        if ((strcmp(argv[1], "help") == 0) || (strcmp(argv[1], "--help") == 0) ||
            (strcmp(argv[1], "--h") == 0) || (strcmp(argv[1], "-h") == 0) ||
            (argc > 2))
        {
            printf("USAGE : sdk_windows_demo [device_name] [logfile]\n");
            printf("   device_name -- Device name (hw:0, hw:2, default, ... Use \"arecord -l\" to list device)\n");
            return 0;
        }
    }

    printf("SDK Windows Demo Version %s\n", SDK_DEMO_VERSION);

    std::string device_name("default");
    std::string logfilename;
    // argv[0] contains the name of the executable
    if (argc > 1)
    {
        std::string arg1(argv[1]);
        device_name = arg1;
    }
    printf(" Using device %s", device_name.c_str());
    if (argc > 2)
    {
        std::string arg2(argv[2]);
        logfilename = arg2;
        printf(" and log file %s", logfilename.c_str());
    }
    printf("\n");

    // Need to initiliaze PortAudio to list all audio devices or to start the intent demo
    PaError err = Pa_Initialize();
    if (err != paNoError)
    {
        printf("Error: Failed to initiliaze PortAudio %d.\n", err); fflush(stdout);
    }
    else
    {
        if (device_name.compare("all") == 0)
        {
            list_all_audio_devices();
        }
        else
        {
            display_what_you_say(device_name, logfilename);

            printf("Demo Stopped\n");
        }

        err = Pa_Terminate();
        if (err != paNoError) {
            printf("Error: Failed to terminate PortAudio %d.\n", err); fflush(stdout);
        }
    }
    return 0;
}

