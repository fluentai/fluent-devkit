// Copyright 2019 Fluent.ai, Inc.

#pragma once

#include <stdint.h>

#if defined(_WIN32) || defined(_WIN64)
	#define FLUENTAI_EXPORT __declspec(dllexport)
#elif defined(__GNUC__) && !defined(__clang__)
	#define FLUENTAI_EXPORT __attribute__ ((visibility ("default"), externally_visible))
#else
	#define FLUENTAI_EXPORT 
#endif

#define FLUENTAI_WORD_STRING_LENGTH 256

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct fluentai_detection_result_type
	{
		char word_string[FLUENTAI_WORD_STRING_LENGTH];
		float confidence;
	} fluentai_detection_result;

	// States of the core state machine
	static const uint8_t FLUENTAI_STATE_WAITING = 0;
	static const uint8_t FLUENTAI_STATE_VOICE_DETECTED = 1;
	static const uint8_t FLUENTAI_STATE_WAKE_WORD_DETECTED = 2;
	static const uint8_t FLUENTAI_STATE_RECORDING_COMMAND = 3;
	static const uint8_t FLUENTAI_STATE_COMMAND_DETECTED = 4;
	static const uint8_t FLUENTAI_STATE_COMMAND_NOT_DETECTED = 5;
	static const uint8_t FLUENTAI_STATE_ERROR = 0xFF;

	/**
	 * Create a core object. 
	 * If dynamic allocation is supported, instance is allocated on heap else object is static.
	 * 
	 * @sa fluentai_deleteInstance
	 *
	 * @return Pointer to created core object.
	 */
	FLUENTAI_EXPORT void* fluentai_createInstance(void);

	/**
	 * Delete a core object. 
	 * If dynamic allocation is supported, instance is deleted from heap else nothing is done.
	 * 
	 * @sa fluentai_createInstance
	 * 
	 * @param[in] coreRef : Reference to the pointer of the core object to delete
	 */
	FLUENTAI_EXPORT void fluentai_deleteInstance(void** coreRef);

	/**
	 * Process audio into the core and update core state. Includes wake word and command detection.
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[in] data : Buffer containing int16_t 16KHz PCM audio sample
	 * @param[in] data_count : Audio buffer size
	 * @return States of the core state machine: \n
	 * - FLUENTAI_STATE_WAITING: Waiting for a wake word to be detected 
	 * - FLUENTAI_STATE_VOICE_DETECTED: Returned when VAD is enabled and speech is detected
	 * - FLUENTAI_STATE_WAKE_WORD_DETECTED: Wake word has been detected and detection result is available
	 * - FLUENTAI_STATE_RECORDING_COMMAND: Recording audio after wake word detection while command is not detected yet
	 * - FLUENTAI_STATE_COMMAND_DETECTED: Command has been detected and detection result is available
	 * - FLUENTAI_STATE_COMMAND_NOT_DETECTED: Command has been not detected in the recorded command audio
	 * - FLUENTAI_STATE_ERROR: Indicate that an error as occured when processing audio into core
	 */	
	FLUENTAI_EXPORT uint8_t fluentai_processAudio(void* core, const int16_t* data, uint32_t data_count);

	/**
	 * Reset core state but keep parameters that have been set and enrollement.  
	 * Useful in case audio processing has been interrupted by a system event and we want to avoid unexpected behaviour when resuming.
	 * 
	 * @param[in] core : Pointer to the core object
	 */		
	FLUENTAI_EXPORT void fluentai_reset(void* core);

	/**
	 * Get the result of the wake words detection
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[out] result : Structure containing detection status, including a string with the detected wake word and confidence
	 */		
	FLUENTAI_EXPORT void fluentai_getDetectedWakeWord(void* core, fluentai_detection_result* result);	

	/**
	 * Get the result of the command detection
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[out] result : Structure containing detection status, including a string with the detected command and confidence
	 */		
	FLUENTAI_EXPORT void fluentai_getDetectedCommand(void* core, fluentai_detection_result* result);

	/**
	 * Get the result of the speaker detection
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[out] result : Structure containing detection status, including a string with the detected speaker and confidence
	 */	
	FLUENTAI_EXPORT void fluentai_getDetectedSpeaker(void* core, fluentai_detection_result* result);	

	/**
	 * Standalone command decoding directly from an audio buffer without using command decoding from core in fluentai_processAudio() 
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[in] data : Buffer containing int16_t 16KHz PCM audio sample
	 * @param[in] data_count : Audio buffer size
	 * @param[out] result : Structure containing detection status, including a string with the detected command and confidence
	 */		
	FLUENTAI_EXPORT void fluentai_decodeCommand(void* core, const int16_t* data, uint32_t data_count, fluentai_detection_result* result);

	/**
	 * Initialize enrollement before using fluentai_enroll()
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[in] name : Name associated with data to enroll. For instance, the name of the wakeword or speaker.
	 * @return Returns true if enrollement has been started.
	 */	
	FLUENTAI_EXPORT uint8_t fluentai_startEnrollment(void* core, const char* name);

	/**
	 * Process audio for enrollement.
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[in] data : Buffer containing int16_t 16KHz PCM audio sample
	 * @param[in] data_count : Audio buffer size
	 */	
	FLUENTAI_EXPORT uint8_t fluentai_enroll(void* core, int16_t* data, uint32_t data_count);

	/**
	 * Terminate enrollement. Call once the audio related to an enrollement has been completly passed to fluentai_enroll()
	 * 
	 * @param[in] core : Pointer to the core object
	 * @return Returns true when enrollement conditions are met. For instance, when wakeword is detected for speaker enrollement.
	 */	
	FLUENTAI_EXPORT uint8_t fluentai_enrollmentDone(void* core);

	/**
	 * Get state and data of the model as data buffer that can be saved.
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[out] data : Internal buffer that contains model data. Passed by pointer to recover address.
	 * @param[out] bytesSize : Model data size in bytes in the internal buffer.
	 */	
	FLUENTAI_EXPORT void fluentai_getModelState(void* core, const char** data, uint32_t* bytesSize);

	/**
	 * Set state and data of the model after loading a saved model. 
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[in] data : Data buffer that contains model data.
	 * @param[in] bytesSize : Model data size in bytes in the data buffer.
	 */	
	FLUENTAI_EXPORT void fluentai_restoreModelState(void* core, const char* data, uint32_t bytesSize);

	/**
	 * Reset the state and data of the model. 
	 * The model will be back to its default untrained state.
	 * 
	 * @param[in] core : Pointer to the core object
	 */	
	FLUENTAI_EXPORT void fluentai_resetModelState(void* core);	

	/**
	 * Set specified value of the core to the specified int32_t
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[in] valueName : Name of the value to set \n
	 * Available values: \n
	 * - ww_vad_enabled : enable voice activity detection while waiting for a detected wake word (disabled by default)
	 * - command_vad_enabled : enable voice activity detection to cut recorded command audio (enabled by default)
	 * @param[in] value : New int32_t value
	 * @param[out] errorCode : Success if 0 else there was error. If NULL is passed, argument is ignored.
	 */	
	FLUENTAI_EXPORT void fluentai_setValue_int(void* core, const char* valueName, int32_t value, uint8_t* errorCode);

	/**
	 * Set specified value of the core to the specified float
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[in] valueName : Name of the value to set \n
	 * Available values: \n
	 * - ww_threshold : set confidence to detect wake word 
	 * - command_threshold : set confidence to detect command
	 * @param[in] value : New float value
	 * @param[out] errorCode : Success if 0 else there was error. If NULL is passed, argument is ignored.
	 */		
	FLUENTAI_EXPORT void fluentai_setValue_float(void* core, const char* valueName, float value, uint8_t* errorCode);

	/**
	 * Get specified value of the core as a int32_t value
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[in] valueName : Name of the value to get \n
	 * Available values: \n
	 * - sample_rate : Expected sample rate of audio input 
	 * - ww_vad_enabled : voice activity detection enabled while waiting for a detected wake word 
	 * - command_vad_enabled : voice activity detection enabled to cut recorded command audio
	 * - command_duration_ms : Duration in milliseconds of the recorded audio for command
	 * @param[out] errorCode: Success if 0 else there was error. If NULL is passed, argument is ignored.
	 * @return int32_t value to get
	 */	
	FLUENTAI_EXPORT int32_t fluentai_getValue_int(void* core, const char* valueName, uint8_t* errorCode);

	/**
	 * Get specified value of the core as a float value
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[in] valueName : Name of the value to get \n
	 * Available values: \n
	 * - ww_threshold : set confidence to detect wake word 
	 * - command_threshold : set confidence to detect command
	 * - frame_confidence : Confidence of the last processed frame
	 * - frame_speaker_confidence: Speaker confidence of the last processed frame
	 * @param[out] errorCode : Success if 0 else there was error. If NULL is passed, argument is ignored.
	 * @return int32_t value to get
	 */		
	FLUENTAI_EXPORT float fluentai_getValue_float(void* core, const char* valueName, uint8_t* errorCode);

	/**
	 * Get specified value of the core as a C string
	 * 
	 * @param[in] core : Pointer to the core object
	 * @param[in] valueName : Name of the value to get \n
	 * Available values: \n
	 * - sdk_version : Code version number of the SDK
	 * - model_name : Name of the model embedded in the SDK including wakeword and intent model if available.
	 * @param[out] errorCode: Success if 0 else there was error. If NULL is passed, argument is ignored.
	 * @return C string value to get
	 */	
	FLUENTAI_EXPORT const char* fluentai_getValue_string(void* core, const char* valueName, uint8_t* errorCode);	

	/**
	 * Return last error message that as occured
	 * 
	 * @return Message as C string
	 */
	FLUENTAI_EXPORT const char* fluentai_getLastErrorMessage(void);	

#ifdef __cplusplus	
}
#endif
