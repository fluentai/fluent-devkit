# sdk\_windows\_demo

This objective of this sdk\_windows\_demo example code is to show how to integrate the Fluent.ai SDK with the microphone to display detected wake-words and intents under Windows.

## Requirements

* Windows 10
* Visual Studio 2019
* Microphone
* Fluent.ai SDK library, as provided with this demo or as provided by Fluent.ai. This should contain the libraries (libfluentai-micro.lib and libfluentai-micro.dll) and the header file (fluent/fluentai-sdk.h).

You must also have the Fluent.ai SDK. Please contact Fluent.ai to get a copy.

This demo depends on the PortAudio library to capture audio from the microphone. A customized version is included in this repositories based on pa\_stable\_v190700\_20210406.tgz and a compiled version of PortAudio in exe/portaudio\_x86.dll and exe/portaudio\_x86.lib.

## How to Build

Open the `sdk_windows_demo/sdk_windows_demo.sln` in Visual Studio 2019 and Build the Solution using Debug or Release configuration.

## How to Use

1. Make sure `libfluentai-micro.dll`, `libfluentai-micro.lib` and all required librairies are located with the sdk\_windows\_demo executable or included in library path.

2. Find the PortAudio microphone device you want to use. If you omit device\_name or specify "default", the default device is used. Run `sdk_windows_demo all` to list available PortAudio devices. Used the index as `0`, `1`, ...

3. Launch sdk\_windows\_demo: ```sdk_windows_demo device_name```.

You can optionally specify a log file to write to with ```sdk_windows_demo device_name logfile.tsv```. The log file will be in tab separated TSV format and contain the ww/intent as well as a timestamp in milliseconds.

4. Speak a wakeword and command into the microphone. The list of supported keywords and commands are provided with the SDK. The demo should output the detected wakeword+command to the console.

## Advanced options

As of now, some advanced options can be changed by editing `sdk_windows_demo.cpp` and recompiling the application.
Available options are:
* `PERIOD_SIZE`: Size of recorded buffer to send to SDK in samples. At 16k sampling rate, a period of 160 means buffer will be send every 10ms.
* `MAX_QUEUE_SIZE`: Maximum number of buffer to keep in memory. If SDK is unable to decode fast enough then the following message will be displayed `AudioDevice: Buffer queue is full, throwing data away`.


