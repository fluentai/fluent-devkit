# sdk\_linux\_demo

This objective of this example code is to show how to integrate the fluent SDK with the microphone to  display detected wake-words and intents.

## Requirements

* Linux OS with ALSA (this has been tested on Ubuntu 16+ and on Raspian)
* Microphone
* Fluent SDK, not provided with this demo.  This should contain the library (libfluentai-micro.so) adn the header file (fluentai-sdk.h).

This demo depends on the ALSA library to capture audio from the microphone.  You must first install the ALSA headers.
```
sudo apt install libasound2-dev
```

You must also have the Fluent SDK.  Please contact fluent.ai to get a copy

## How to build

```
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DFLUENTAI_SDK_ROOT=/path/to/sdk
make
```

## How to use

1. Make sure `libfluentai-sdk.so` is in your library path:
`export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/path/to/sdk/lib`

2. Find the ALSA microphone device you want to use. If you omit device\_name or specify "default", the default device is used.  Run `arecord -L` to list available ALSA devices. Usual names are `hw:0`, `plughw:0`, ...

3. Launch sdk\_linux\_demo: ```sdk_linux_demo device_name```.

You can optionally specify a log file to write to with ```sdk_linux_demo device_name logfile.tsv```.  The log file will be in tab separated TSV format and contain the ww/intent as well as UNIX timestamp in milliseconds.

4. Speak a wakeword and command into the microphone.  The list of supported keywords and commands are provided with the SDK.  The demo should output the detected wakeword+command to the console.

## Advanced options

As of now, some advanced options can be changed by editing `sdk_linux_demo.cpp` and recompiling the application.
Available options are:
* `PERIOD_SIZE`: Size of recorded buffer to send to SDK (in samples. At 16k sampling rate, a period of 160 means buffer will be send every 10ms)
* `MAX_QUEUE_SIZE`: Maximum number of buffer to keep in memory. If SDK is unable to decode fast enough, following message will be displayed `AudioDevice: Buffer queue is full, throwing data away`

