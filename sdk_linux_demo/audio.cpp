#include "audio.h"

#include <algorithm>
#include <cerrno>
#include <string>
#include <iostream>


AudioDevice::AudioDevice()
{
    is_live_init_ = false;
    is_recording_ = false;

    handle_ = nullptr;
}

AudioDevice::~AudioDevice()
{
    is_recording_ = false;

    Stop();

    if (handle_ != nullptr)
    {
        snd_pcm_close(handle_);
    }
    // params_ is automatically freed when goes out of scope (inner works of snd_pcm)

    // data_buffer_ is automatically freed when the object is destructed (unique_ptr)
}

void AudioDevice::SetupLive(uint32_t sampling_rate, int32_t period_size, std::string device_name)
{
    int32_t nDir; // Unused but required by some functions

    // Open PCM device for capture
    int32_t return_code = snd_pcm_open(&handle_, device_name.c_str(), SND_PCM_STREAM_CAPTURE, 0);
    if (return_code < 0) throw(std::string("Unable to open pcm device ") + device_name + ": " + snd_strerror(return_code));

    // Allocate hardware parameters object
    snd_pcm_hw_params_alloca(&params_);

    // Fill default values
    return_code = snd_pcm_hw_params_any(handle_, params_);
    if (return_code < 0) throw(std::string("No configuration available: ") + snd_strerror(return_code));

    // Interleaved mode
    return_code = snd_pcm_hw_params_set_access(handle_, params_, SND_PCM_ACCESS_RW_INTERLEAVED);
    if (return_code < 0) throw(std::string("Interleaved access mode not supported: ") + snd_strerror(return_code));

    // Signed 16-bit little endian
    return_code = snd_pcm_hw_params_set_format(handle_, params_, SND_PCM_FORMAT_S16_LE);
    if (return_code < 0) throw(std::string("Sample format not supported: ") + snd_strerror(return_code));

    // Get minimum number of channels
    return_code = snd_pcm_hw_params_set_channels_min(handle_, params_, &num_channels_);
    if (return_code < 0)
    {
        throw(std::string("Channel count unknown: ") + snd_strerror(return_code));
    }

    // Set the number of channels
    return_code = snd_pcm_hw_params_set_channels(handle_, params_, num_channels_);
    if (return_code < 0)
    {
        throw("Channel count (" + std::to_string(num_channels_) + ") not supported: " + snd_strerror(return_code));
    }

    // Sampling rate
    sampling_rate_ = sampling_rate;
    return_code = snd_pcm_hw_params_set_rate_near(handle_, params_, &sampling_rate_, &nDir);
    if (return_code < 0)
    {
        throw("Sampling rate (" + std::to_string(sampling_rate_) + ") not supported: " + snd_strerror(return_code));
    }

    // Period size in frames
    period_size_ = static_cast<snd_pcm_uframes_t>(period_size);
    return_code = snd_pcm_hw_params_set_period_size_near(handle_, params_, &period_size_, &nDir);
    if (return_code < 0)
    {
        throw("Period size (" + std::to_string(period_size_) + ") not supported: " + snd_strerror(return_code));
    }
    // Get the actual value that was set
    return_code = snd_pcm_hw_params_get_period_size(params_, &period_size_, &nDir);
    if (return_code < 0)
    {
        throw(std::string("Unable to get period size: ") + snd_strerror(return_code));
    }

    // Write parameters to driver
    return_code = snd_pcm_hw_params(handle_, params_);
    if (return_code < 0)
    {
        throw(std::string("Unable to set hardware parameters") + snd_strerror(return_code));
    }

    // Buffer size
    buffer_size_ = period_size_ * (bits_per_sample_/8) * num_channels_; // 2 bytes per sample

    is_live_init_ = true;
}

void AudioDevice::Record(uint32_t max_queue_size)
{
    if (!is_live_init_)
    {
        throw("Live recording hardware was not initialized, please call SetupLive before recording");
    }

    max_queue_size_ = max_queue_size;

    is_recording_ = true;
    record_thread_ = std::thread(&AudioDevice::RecordThreadFunction, this); // Spawn child thread to record
}

void AudioDevice::Stop()
{
    if (is_recording_)
    {
        is_recording_ = false;
        record_thread_.join();
        snd_pcm_drop(handle_);
    }
}


void AudioDevice::GetRecordedData(std::vector<int16_t>& buffer) {
    // Create a lock on record_mutex_ & lock it
    std::unique_lock<std::mutex> record_lock(record_mutex_);

    if (!is_recording_ && data_buffer_.empty())
    {
        throw("No recorded data available and not recording anymore");
    }

    while (data_buffer_.empty() && is_recording_)
    {
        // Check if there's data available, and if not wait for it
        record_cond_.wait(record_lock);
    }

    int32_t converted_buffer_size = buffer_size_ / (bits_per_sample_/8) / num_channels_; // Size of the buffer after reinterpret cast to fl_int16
    int32_t total_samples = converted_buffer_size * data_buffer_.size();

    buffer.resize(total_samples);

    for (int32_t i = 0; !data_buffer_.empty(); i++)
    {
        // Dump the current buffer to the matrix
        int16_t *data = reinterpret_cast<int16_t*>(&(data_buffer_.front())[0]);
        for (int32_t j = 0; j < converted_buffer_size; j++)
        {
            // Use first channel data
            buffer[i * converted_buffer_size + j] = data[j * num_channels_ + 0/*channel 0*/];
        }

        data_buffer_.pop();
    }

    record_lock.unlock();
}

void AudioDevice::RecordThreadFunction()
{
    // Create a lock on record_mutex_
    std::unique_lock<std::mutex> record_lock(record_mutex_, std::defer_lock);

    while (is_recording_)
    {
        std::vector<char> buffer(buffer_size_);
        int return_code = snd_pcm_readi(handle_, &buffer[0], period_size_);
        if (return_code == -EPIPE)
        {
            throw("Overrun occured during recording");
        }
        else if (return_code < 0)
        {
            std::cerr << "AudioDevice: Error occured during recording: " << snd_strerror(return_code) << std::endl;
            std::cerr << "AudioDevice: Stop audio recording." << std::endl;
            break;
        }
        else if (return_code < static_cast<int32_t>(period_size_))
        {
            std::cout << "AudioDevice: Short read - read " << return_code << " frames instead of " << period_size_ << std::endl;
            std::cout << "AudioDevice: Unexpected data len returned by ALSA, throwing data away" << std::endl;
            continue;
        }

        record_lock.lock();
        // Put current data into data buffer
        data_buffer_.push(std::move(buffer));

        // If data buffer is full, remove the first element
        if (data_buffer_.size() > max_queue_size_)
        {
            std::cout << "AudioDevice: Buffer queue is full, throwing data away" << std::endl;
            data_buffer_.pop();
        }

        // Queue was empty, notify that it's not anymore
        if (data_buffer_.size() == 1)
        {
            record_lock.unlock();
            record_cond_.notify_all();
        }
        else
            record_lock.unlock();
    }
}

