#ifndef _FL_BASE_AUDIO_H_
#define _FL_BASE_AUDIO_H_

#include <alsa/asoundlib.h>

#include <cstdint>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>


//! Linux specific class used to record from the microphone and load WAVE files.
/*!
    This class is mainly used to record from the microphone for Linux applications.
    It uses ALSA library for recording, kaldi library for reading from WAVE files.
    FLAC is not currently supported.
*/
class AudioDevice
{
public:
    AudioDevice();
    ~AudioDevice();

    //! Configuration flags for AudioDevice's live setup.
    enum AUDIO_CONFIG
    {
        //! Read/Write files of type WAVE.
        AUDIO_FILETYPE_WAV,
        //! Read/Write files of type FLAC.
        AUDIO_FILETYPE_FLAC
    };

    //! Setup the underlying audio handle for live recording.
    /*!
        \param sampling_rate Sampling rate of the recording, default to 16000.
        \param period_size Number of frames per recording period, default to 160 (10 ms at 16000 Hz).
        \param device_name ALSA device name, e.g. "hw:1", "default".  See http://www.alsa-project.org/alsa-doc/alsa-lib/pcm.html
        \throw IOException on error or invalid settings
    */
    void SetupLive(uint32_t sampling_rate = 16000, int32_t period_size = 160, std::string device_name="default");

    //! Record from microphone.
    /*!
        Start recording from the microphone.
        Function will start the recording on a child thread and return immediately.

        \param max_queue_size_ Maximum buffer queue size, above that data will get thrown out. Default is 20 (200 ms for 16000 Hz & 10 ms per buffer).
        \throw IOException on error
    */
    void Record(uint32_t max_queue_size = 20);

    //! Stop recording.
    /*!
        Stop and join the recording thread, save any unsaved data.
        \sa Record_Thread().
    */
    void Stop();

    //! Return whether the device is recording.
    bool IsRecording() const
    {
        return is_recording_;
    }

    //! Empties the buffer queue into wave_buffer.
    /*!
        Blocks until data is available.
        \param wave_buffer Reference to a WaveData that will be filled with available data.
        \throw InputException if not recording
    */
    void GetRecordedData(std::vector<int16_t>& buffer);
    //! Recording function run by the recording thread.
    //! \throw IOException on error
    void RecordThreadFunction();

    //! Structure representing the RIFF header at the beginning of WAVE files.
    /*
    struct RIFFheader //                                    Bytes   Total
    {
        char        chunkID[4];             // "RIFF"          4       4
		std::uint32_t    riff_size;         // filesize - 8    4       8
        char        typeID[4];              // "WAVE"          4       12
        char        formatChunkID[4];       // "fmt "          4       16
		std::uint32_t    format_chunk_size; // 16 bytes        4       20
        std::uint16_t    format_tag;        //                 2       22
        std::uint16_t    num_channels;      //                 2       24
        std::uint32_t    samples_per_sec;   //                 4       28
        std::uint32_t    bytes_per_sec;     //                 4       32
        std::uint16_t    block_align;       //                 2       34
        std::uint16_t    bits_per_sample;   //                 2       36
        char        dataChunkID[4];         // data"           4       40
		std::uint32_t    data_chunk_size;   // not fixed       4       44
    };
    */
private:

    //! Whether underlying audio component is initialized.
    bool is_live_init_;
    //! Whether it is recording.
    bool is_recording_;

    //! Audio hardware handle.
    snd_pcm_t *handle_;
    //! Audio hardware parameters.
    snd_pcm_hw_params_t *params_;
    //! Size of 1 recording period in number of frames.
    snd_pcm_uframes_t period_size_;

    //! Sampling rate of recording.
    uint32_t sampling_rate_;
    //! Number of channels
    uint32_t num_channels_ = 1;
    //! Bits per sample, hardcoded to 16bits little endian.
    const int32_t bits_per_sample_ = 16; // fixed 16bits little endian.

    //! Size of 1 buffer (for 1 period).
    uint32_t buffer_size_;
    //! Maximum number of buffers to keep in the queue.
    uint32_t max_queue_size_;
    //! Queue of buffers that hold recorded data.
    std::queue<std::vector<char>> data_buffer_;


    //! Child thread running the recording.
    std::thread record_thread_;
    //! Mutex for accessing the buffer queue.
    std::mutex record_mutex_;
    //! Conditional variables used to wait when the queue is empty.
    std::condition_variable record_cond_;
};

#endif /* _FL_BASE_AUDIO_H_ */
