#include <fluent/fluentai-sdk.h>
#include "audio.h"
#include <iostream>
#include <string>
#include <chrono>
#include <fstream>
#include <signal.h>

// Demo version
#define SDK_DEMO_VERSION "1.0.2"

// Configuration
const int32_t PERIOD_SIZE = 160;
const uint32_t MAX_QUEUE_SIZE = 100;

#define MSG_PREFIX "(Fluent.ai)  "
void PRINTF(const char* format, ...) {
    printf(MSG_PREFIX);
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
}

class DemoResouce {
public:
    virtual bool is_available_ww(fluentai_detection_result &result) { return true;};
    virtual bool is_available_air(fluentai_detection_result &result) { return true;};
    void print_welcome() {
        std::string msg = "================================================================================\n"
                        MSG_PREFIX "Fluent.ai, Montreal, Canada                                2024-1-8\n"
                        MSG_PREFIX "\n"
                        MSG_PREFIX "Demonstration using Raspberry Pi with Fluent.ai model.\n"
                        MSG_PREFIX "\n" +
                        get_msg_of_model_support() +
                        MSG_PREFIX "===================================================================\n"
                        MSG_PREFIX "\n" 
                        ;
        std::cout << msg;
    }
protected:
    virtual std::string get_msg_of_model_support() {
        return MSG_PREFIX "   For this demo: Hi Anita is the wake word.\n"
               MSG_PREFIX "\n"
               ;
    }

};

static bool compare_strings_ignore_whitespace(const char* str1, const char* str2) {
    while (*str1 != '\0' && *str2 != '\0') {
        while (isspace((unsigned char)*str1)) str1++;
        while (isspace((unsigned char)*str2)) str2++;
        if (*str1 != *str2) {
            return false;
        }
        if (*str1) str1++;
        if (*str2) str2++;
    }
    while (isspace((unsigned char)*str1)) str1++;
    while (isspace((unsigned char)*str2)) str2++;
    return *str1 == '\0' && *str2 == '\0';
}

class KoreanWWCommandsDemoResouce :public DemoResouce {
public:
    virtual bool is_available_ww(fluentai_detection_result &result) { 
        //Only accept Hey google and Hey Siri.
        return result.command_id > 0;
    };
    virtual bool is_available_air(fluentai_detection_result &result) { 
        //Only accept the below commands.
        const char *list_korean_command[] = {
        "{\"action\": \"activate\", \"object\": \"intercom one\"}",
        "{\"action\": \"activate\", \"object\": \"intercom three\"}",
        "{\"action\": \"activate\", \"object\": \"intercom two\"}",
        "{\"action\": \"answer\", \"object\": \"call\"}",
        "{\"action\": \"check\", \"object\": \"battery\"}",
        "{\"action\": \"check\", \"object\": \"camera status\"}",
        "{\"action\": \"ignore\", \"object\": \"call\"}",
        "{\"action\": \"mute\", \"object\": \"microphone\"}",
        "{\"action\": \"next\"}",
        "{\"action\": \"pair\", \"object\": \"intercom\"}",
        "{\"action\": \"pair\", \"object\": \"phone\"}",
        "{\"action\": \"play music\"}",
        "{\"action\": \"previous\"}",
        "{\"action\": \"redial\"}",
        "{\"action\": \"speed dial\", \"object\": \"one\"}",
        "{\"action\": \"speed dial\", \"object\": \"three\"}",
        "{\"action\": \"speed dial\", \"object\": \"two\"}",
        "{\"action\": \"start\", \"object\": \"recording\"}",
        "{\"action\": \"stop music\"}",
        "{\"action\": \"stop\", \"object\": \"recording\"}",
        "{\"action\": \"tag\", \"object\": \"video\"}",
        "{\"action\": \"turn down\", \"object\": \"volume\"}",
        "{\"action\": \"turn off\", \"object\": \"ambient mode\"}",
        "{\"action\": \"turn off\", \"object\": \"camera\"}",
        "{\"action\": \"turn off\", \"object\": \"flashlight\"}",
        "{\"action\": \"turn off\", \"object\": \"fm radio\"}",
        "{\"action\": \"turn off\", \"object\": \"intercom\"}",
        "{\"action\": \"turn off\", \"object\": \"noise cancelling\"}",
        "{\"action\": \"turn off\", \"object\": \"tail light\"}",
        "{\"action\": \"turn on\", \"object\": \"ambient mode\"}",
        "{\"action\": \"turn on\", \"object\": \"camera\"}",
        "{\"action\": \"turn on\", \"object\": \"flashlight\"}",
        "{\"action\": \"turn on\", \"object\": \"fm radio\"}",
        "{\"action\": \"turn on\", \"object\": \"noise cancelling\"}",
        "{\"action\": \"turn on\", \"object\": \"tail light\"}",
        "{\"action\": \"turn up\", \"object\": \"volume\"}",
        "{\"action\": \"unmute\", \"object\": \"microphone\"}"
        };
        #define NB_ENTRY_LIST_KOREAN_COMMAND (sizeof(list_korean_command) / sizeof(list_korean_command[0]))
        for (int i = 0; i < NB_ENTRY_LIST_KOREAN_COMMAND; i++)
        {

            // PRINTF("\n[%s] vs\n[%s]\n", result.word_string,list_korean_command[i]);
            if (compare_strings_ignore_whitespace(list_korean_command[i], result.word_string))
            {
                return true;
            }
        }
        PRINTF("Unsupported cmd: %s\n", result.word_string);
        return false;
    };
    virtual std::string get_msg_of_model_support() {
        return MSG_PREFIX "For this demo: \"Hey Google\" or \"Hey Siri\" are the wake words.\n"
               MSG_PREFIX "\n"
               ;
    }

};

#define KOREAN_WW_COMMANDS 1
#define GENERIC_MODEL 2

#if MODEL_TYPE == KOREAN_WW_COMMANDS
    KoreanWWCommandsDemoResouce g_demo_resource;
#elif MODEL_TYPE == GENERIC_MODEL
    DemoResouce g_demo_resource;
#else
    #error "Model type not defined"
#endif


// Action used to stop the demo.
// Use none or an invalid action name for no stop action
#define DEMO_STOP_ACTION "none"

// Enable the following define to support multiple command mode
//#define ENABLE_MULTIPLE_COMMAND

// Allow to stop application nicely by catching Ctrl-C signal
bool stop = false;
void sigint_handler(int /*signal*/)
{
    PRINTF("\n  Ctrl-C detected...\n");
    stop = true;
}

void parse_command(const char *p_command, char *p_action, char *p_object)
{
    int str_index = 0;
    int substr_index = 0;
    enum parsing_state_e {
        PARSING_ACTION,
        PARSING_ACTION_FOUND,
        PARSING_OBJECT,
        PARSING_OBJECT_FOUND,
        PARSING_DONE
    } parsing_state = PARSING_ACTION;

    p_action[0] = '\0';
    p_object[0] = '\0';
    while (p_command[str_index] != '\0')
    {
        if (p_command[str_index] == '"')
        {
            // Skip these characters
            str_index++;
            continue;
        }

        switch (parsing_state)
        {
            case PARSING_ACTION:
                if (p_command[str_index] == ' ')
                {
                    // Skip these characters
                    str_index++;
                    continue;
                }
                else if (p_command[str_index] == ':')
                {
                    parsing_state = PARSING_ACTION_FOUND;
                    substr_index = 0;
                }
                break;
            case PARSING_ACTION_FOUND:
                if ((p_command[str_index] == ',') || (p_command[str_index] == '}'))
                {
                    parsing_state = PARSING_OBJECT;
                    p_action[substr_index] = '\0';
                }
                else
                {
                    p_action[substr_index++] = p_command[str_index];
                }
                break;
            case PARSING_OBJECT:
                if (p_command[str_index] == ' ')
                {
                    // Skip these characters
                    str_index++;
                    continue;
                }
                else if (p_command[str_index] == ':')
                {
                    parsing_state = PARSING_OBJECT_FOUND;
                    substr_index = 0;
                }
                break;
            case PARSING_OBJECT_FOUND:
                if (p_command[str_index] == '}')
                {
                    parsing_state = PARSING_DONE;
                    p_object[substr_index] = '\0';
                }
                else
                {
                    p_object[substr_index++] = p_command[str_index];
                }
                break;
            default:
                // Nothing to do
                break;
        } // end switch
        str_index++;
    } // end while
}

void display_what_you_say(std::string device_name, std::string logfilename)
{
    g_demo_resource.print_welcome();
    // Setup Fluent SDK
    void *sdk = fluentai_createInstance();
    std::string sdk_version = fluentai_getValue_string(sdk, "sdk_version", nullptr);
    std::string model_name = fluentai_getValue_string(sdk, "model_name", nullptr);
    int32_t sample_rate = fluentai_getValue_int(sdk, "sample_rate", nullptr);
    PRINTF("Loading SDK: %s : MODEL: %s\n", sdk_version.c_str(), model_name.c_str());
    PRINTF("Starting audio with requested sample rate %d Hz\n", sample_rate);

    // Optionally set confidence thresholds for WW and intent recognition. Otherwise, it will use the default values.
    //
    // fluentai_setValue_float(sdk, "ww_threshold", 0.95, nullptr);
    // fluentai_setValue_float(sdk, "command_threshold", 0.5, nullptr);
#ifdef ENABLE_MULTIPLE_COMMAND
    // By default, multiple commands is disabled
    fluentai_setValue_int(sdk, "multiple_command", 1, nullptr);
#endif

    // Setup audio device
    AudioDevice device;
    try
    {
        device.SetupLive(sample_rate, PERIOD_SIZE, device_name);
    }
    catch (std::string &s)
    {
        PRINTF("Error during setup: %s\n", s.c_str());
        fflush(stdout);
        return;
    }
    device.Record(MAX_QUEUE_SIZE);
    std::vector<int16_t> buffer;

    // Allow clean stop using Ctrl-C
    signal(SIGINT, sigint_handler);

    std::ofstream logfile;
    if (!logfilename.empty())
    {
        logfile.open(logfilename);
        logfile << "ww_timestamp\tww\tww_confidence\tintent_timestamp\tintent\tintent_confidence" << std::endl;
    }
    bool waiting_for_command = false;

    PRINTF("Demo is now running.  Please speak normally into the microphone.\n");
    PRINTF("\n");
    fflush(stdout);

    // Start recording
    try
    {
        int prev_status = FLUENTAI_STATE_WAITING;
        while (!stop)
        {
            // Get last recorder frame
            device.GetRecordedData(buffer);

            // Detect wake phrase or post commands
            int status = fluentai_processAudio(sdk, &buffer[0], buffer.size());
            if (status != prev_status)
            {
                switch (status)
                {
                    case FLUENTAI_STATE_WAITING:
                        // Nothing to do waiting for wake word
                        break;

                    case FLUENTAI_STATE_RECORDING_COMMAND:
                        // Nothing to do wake word detected recording command
                        break;

                    case FLUENTAI_STATE_ERROR:
                    {
                        std::string message = fluentai_getLastErrorMessage();
                        PRINTF("\nError processing audio : %s\n", message.c_str());
                        fflush(stdout);
                        stop = true;
                        break;
                    }

#ifdef ENABLE_MULTIPLE_COMMAND
                    case FLUENTAI_STATE_COMMAND_COMPLETED:
                        // Nothing to do multiple commands were detected
                        waiting_for_command = false;
                        PRINTF("Multiple commands completed\n"); fflush(stdout);
                        break;
#endif

                    case FLUENTAI_STATE_WAKE_WORD_DETECTED:
                    {
                        if (waiting_for_command)
                        {
                            if (logfile.is_open()) logfile << "\t\t\n";
                            PRINTF("  Command was not completed!\n");
                            fflush(stdout);
                        }
                        fluentai_detection_result result;
                        fluentai_getDetectedWakeWord(sdk,&result);
                        if (!g_demo_resource.is_available_ww(result)){
                            break;
                        }
                        auto timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
                        std::time_t time = std::time(nullptr);
                        std::string timestr = std::asctime(std::localtime(&time));
                        timestr.pop_back(); // remove final newline
                        PRINTF("WW  Confidence %07.04f%%", result.confidence * 100);
                        printf(" -- %s\n",  result.word_string); 
                        fflush(stdout);

                        if (logfile.is_open())
                        {
                            logfile << timestamp << '\t' << result.word_string << '\t' << result.confidence << '\t' << std::flush;
                        }
                        waiting_for_command = true;
                        break;
                    }

                    case FLUENTAI_STATE_COMMAND_DETECTED:
                    {
                        char action_str[FLUENTAI_WORD_STRING_LENGTH];
                        char object_str[FLUENTAI_WORD_STRING_LENGTH];
                        auto timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
                        fluentai_detection_result result;
                        fluentai_getDetectedCommand(sdk,&result);
                        parse_command(result.word_string, action_str, object_str);
                        if (!g_demo_resource.is_available_air(result)){
                            break;
                        }
                        PRINTF("AIR Confidence %07.04f%%   %s %s\n", result.confidence * 100, action_str, object_str);
                        fflush(stdout);
                        if (logfile.is_open())
                        {
                            logfile << timestamp << '\t' << result.word_string << '\t' << result.confidence << '\n' << std::flush;
                        }

#ifndef ENABLE_MULTIPLE_COMMAND
                        waiting_for_command = false;
#endif

                        if (strstr(action_str, DEMO_STOP_ACTION) != nullptr)
                        {
                            // If the word stop is spoken then exit demo
                            PRINTF("\n  Stop action detected...\n");
                            stop = true;
                        }
                        break;
                    }

                    case FLUENTAI_STATE_COMMAND_NOT_DETECTED:
                    {
                        PRINTF("Command was not recognized!\n");
                        if (logfile.is_open()) logfile << "\t\t\n";
                        waiting_for_command = false;
                        break;
                    }

                    default:
                    {
                        std::string message = fluentai_getLastErrorMessage();
                        PRINTF("\nUnsupported status : %d\n", status);
                        PRINTF("Last error processing audio : %s\n", message.c_str());
                        fflush(stdout);
                        stop = true;
                        break;
                    }
                } // end switch

                prev_status = status;

            } // end if
        } // end while
    }
    catch (std::string &s)
    {
        PRINTF("\nError while listening: %s\n", s.c_str());
        fflush(stdout);
    }

    // Cleaning
    PRINTF("Shutting down...\n");
    fflush(stdout);

    if (logfile.is_open())
    {
        if (waiting_for_command) logfile << "\t\t\n";
        logfile.close();
        waiting_for_command = false;
    }
    device.Stop();
    fluentai_deleteInstance(&sdk);
}

int main(int argc, char *argv[])
{
    auto start_time = std::chrono::high_resolution_clock::now();
    if (argc > 1)
    {
        if ((strcmp(argv[1], "help") == 0) || (strcmp(argv[1], "--help") == 0) ||
            (strcmp(argv[1], "--h") == 0) || (strcmp(argv[1], "-h") == 0) ||
            (argc > 2))
        {
            PRINTF("USAGE : sdk_windows_demo [device_name] [logfile]\n");
            PRINTF("   device_name -- Device name (hw:0, hw:2, default, ... Use \"arecord -l\" to list device)\n");
            return 0;
        }
    }

    PRINTF("SDK Linux Demo Version %s\n", SDK_DEMO_VERSION);

    std::string device_name("default");
    std::string logfilename;
    // argv[0] contains the name of the executable
    if (argc > 1)
    {
        std::string arg1(argv[1]);
        device_name = arg1;
    }
    PRINTF("Using device %s", device_name.c_str());
    if (argc > 2)
    {
        std::string arg2(argv[2]);
        logfilename = arg2;
        PRINTF(" and log file %s", logfilename.c_str());
    }
    PRINTF("\n");

    display_what_you_say(device_name, logfilename);

    PRINTF("Demo Stopped\n");

    return 0;
}
