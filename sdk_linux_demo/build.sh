#!/bin/bash
set -e
# Function to display usage
usage() {
    echo "Usage: $0 [MODEL_TYPE] [FLUENTAI_SDK_PATH]"
    echo "MODEL_TYPE options:"
    echo "    KOREAN_WW_COMMANDS - Build Korean WW Commands model"
    echo "    GENERIC_MODEL - Build Generic model"
    exit 1
}

# Check if the correct number of arguments is provided
if [ "$#" -ne 2 ]; then
    usage
fi

# Assign arguments to variables
MODEL_TYPE=$1
FLUENTAI_SDK_PATH=$2
FLUENTAI_SDK_PATH=/home/pi/pc_demo/projects/ko-KR_final_Nov_15/sdk_native/Linux_aarch64/

# Check if the provided model type is valid
if [ "$MODEL_TYPE" != "KOREAN_WW_COMMANDS" ] && [ "$MODEL_TYPE" != "GENERIC_MODEL" ]; then
    echo "Invalid model type: $MODEL_TYPE"
    usage
fi

# Create a build directory and navigate into it
mkdir -p build
cd build

# Run CMake with the provided arguments
if [ "$MODEL_TYPE" = "KOREAN_WW_COMMANDS" ]; then
    echo cmake .. -DCMAKE_BUILD_TYPE=Release -DUSE_KOREAN_WW_COMMANDS=ON -DFLUENTAI_SDK_ROOT="$FLUENTAI_SDK_PATH" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
    cmake .. -DCMAKE_BUILD_TYPE=Release -DUSE_KOREAN_WW_COMMANDS=ON -DFLUENTAI_SDK_ROOT="$FLUENTAI_SDK_PATH" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
elif [ "$MODEL_TYPE" = "GENERIC_MODEL" ]; then
    cmake .. -DCMAKE_BUILD_TYPE=Release -DUSE_GENERIC_MODEL=ON -DFLUENTAI_SDK_ROOT="$FLUENTAI_SDK_PATH" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
fi

# Build the project
make
